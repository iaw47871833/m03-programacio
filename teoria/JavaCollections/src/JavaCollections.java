import java.util.ArrayList;
import java.util.Iterator;

public class JavaCollections {

	public static void main(String[] args) {
		
			// Creem l'array list
			ArrayList<String> l1 = new ArrayList<String>();
			
			// Afegim dos animals a la llista
			l1.add("Gat");
			l1.add("jaguar");
			
			System.out.println(l1.get(0));
			System.out.println(l1.get(1));
			
			// Modifiquem l'animal de la posició 0
			l1.set(0, "Gos");
			
			System.out.println(l1.get(0));
			System.out.println(l1.get(1));
			
			// Afegim més animals
			l1.add("pantera");
			l1.add("gat");
			l1.add("lynx");
			
			// Recorrem tota la llista amb un bucle
			for (int i = 0; i < l1.size(); i++) {
				System.out.println("\nAnimal " + i + " : " + l1.get(i));
			}
			
			// Mirem si l'animal lynx està a la llista
			if (l1.contains("lynx")) {
				System.out.println("Lynx està present a la llista");
			} else {
				System.out.println("Lynx no està present a la llista");
			}
			
			// For in
			for (String animal: l1) {
				System.out.println(animal);
			}
			
			// Iterator
			System.out.println("Iterator");
			/*
			Iterator i = l1.iterator();
			while (i.hasNext()) {
				System.out.println(i.next());
			}*/
			
			/*Iterator i = l1.iterator();
			while (i.hasNext() ) {
				System.out.println(i.next());
			}*/
			
			String s[] = new String[l1.size()];
	}
}
