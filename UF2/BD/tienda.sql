-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 06, 2021 at 01:03 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tienda`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getClientById` (IN `clientId` INT)  select * from clientes where id = clientId$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getClients` ()  select * from clientes$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getProductById` (IN `productId` INT)  select * from productos where id = productId$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getProducts` ()  SELECT * from productos$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`, `direccion`) VALUES
(1, 'Lionel', 'Valencia'),
(2, 'Sergi', 'Valencia'),
(3, 'Alfredo', 'Valencia'),
(4, 'Maria', 'Valencia'),
(5, 'Maria', 'Alicante'),
(6, 'Pepito', 'Alicante'),
(7, 'Luis', 'Elx'),
(8, 'Antonia', 'Xàtiva'),
(9, 'Carmen', 'Paterna'),
(10, 'alex', 'barcelona'),
(14, 'Ana', 'Barcelona');

-- --------------------------------------------------------

--
-- Table structure for table `comanda`
--

CREATE TABLE `comanda` (
  `idCliente` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precioTotal` float DEFAULT NULL,
  `fechaComanda` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comanda`
--

INSERT INTO `comanda` (`idCliente`, `idProducto`, `cantidad`, `precioTotal`, `fechaComanda`) VALUES
(1, 1, 1, 30, '2021-03-04 20:24:36'),
(7, 4, 2, 60, '2021-03-04 20:26:49'),
(9, 2, 2, 60, '2021-03-04 20:25:42'),
(10, 3, 2, 200, '2021-03-04 20:22:02'),
(14, 5, 1, 90, '2021-03-04 20:23:25');

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `descripcion`, `precio`) VALUES
(1, 'camiseta larga', 'camiseta larga del bershka', 30),
(2, 'pantalones tejanos', 'pantalones tejanos rotos por las rodillas', 25),
(3, 'camiseta supreme', 'camiseta supreme con colores chillones', 120),
(4, 'chaqueta bomber', 'chaqueta bomber color negra con pin blanco', 45),
(5, 'zapatos adidas tempo', 'zapatos adidas de deporte color gris', 100);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comanda`
--
ALTER TABLE `comanda`
  ADD PRIMARY KEY (`idCliente`,`idProducto`),
  ADD KEY `idProducto` (`idProducto`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comanda`
--
ALTER TABLE `comanda`
  ADD CONSTRAINT `comanda_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `comanda_ibfk_2` FOREIGN KEY (`idProducto`) REFERENCES `productos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
