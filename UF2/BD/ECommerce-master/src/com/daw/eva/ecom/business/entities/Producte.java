package com.daw.eva.ecom.business.entities;

public class Producte {
	// Atributs
	private long id;
	private String nombre;
	private String direccion;
	private double precio;
	
	/**
	 * @param nombre
	 * @param direccion
	 * @param precio
	 */
	public Producte(String nombre, String direccion, double precio) {
		this.nombre = nombre;
		this.direccion = direccion;
		this.precio = precio;
	}

	/**
	 * @param id
	 * @param nombre
	 * @param direccion
	 * @param precio
	 */
	public Producte(long id, String nombre, String direccion, double precio) {
		this.id = id;
		this.nombre = nombre;
		this.direccion = direccion;
		this.precio = precio;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * @return the precio
	 */
	public double getPrecio() {
		return precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(double precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Producte [id=" + id + ", nombre=" + nombre + ", direccion=" + direccion + ", precio=" + precio + "]";
	}
	
	
}
