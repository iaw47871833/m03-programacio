package com.daw.eva.ecom.business.entities;

public class Client {
	// id, nombre, direccion
	private long id;
	private String nombre;
	private String direccion;
	
	// Constructor
	public Client(int id, String nombre, String direccion) {
		this.id = id;
		this.nombre = nombre;
		this.direccion = direccion;
	}
	
	public Client(String nombre, String direccion) {
		this.nombre = nombre;
		this.direccion = direccion;
	}

	public long getId() {
		return id;
	}

	public void setId(long l) {
		this.id = l;
	}

	// Getters i setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Override
	public String toString() {
		return "Client [nombre=" + nombre + ", direccion=" + direccion + "]";
	}
}
