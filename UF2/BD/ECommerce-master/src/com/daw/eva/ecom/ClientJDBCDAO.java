package com.daw.eva.ecom;


import java.sql.DriverManager;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.daw.eva.ecom.business.entities.Client;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

/**
 *
 * @author eva
 * TODO
 * Cal definir els contractes de la interfície ClientDAO i demés entitats, i implementar-les 
 * Cal crear de la mateixa manera, ProducteJDBCDAO que implementa ProducteDAO, i idem per a ComandaJDBCDAO
 */
public class ClientJDBCDAO implements ClientDAO {

    // Conexión a la base de datos
    private static Connection conn = null;

    // Configuración de la conexión a la base de datos
    private static final String DB_HOST = "localhost";
    private static final String DB_PORT = "3307";
    private static final String DB_NAME = "tienda";
    private static final String DB_URL = "jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "?serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "password";
    private static final String DB_MSQ_CONN_OK = "CONEXIÓN CORRECTA";
    private static final String DB_MSQ_CONN_NO = "ERROR EN LA CONEXIÓN";

    // Configuración de la tabla Clientes
    private static final String DB_CLI = "clientes";
    private static final String DB_CLI_SELECT = "SELECT * FROM " + DB_CLI;
    private static final String DB_CLI_ID = "id";
    private static final String DB_CLI_NOM = "nombre";
    private static final String DB_CLI_DIR = "direccion";
    private static Connection connection = null;

    //////////////////////////////////////////////////
    // MÉTODOS DE CONEXIÓN A LA BASE DE DATOS
    //////////////////////////////////////////////////
    ;
    
    /**
     * Intenta cargar el JDBC driver.
     * @return true si pudo cargar el driver, false en caso contrario
     */
    public static boolean loadDriver() {
        try {
            System.out.print("Loading Driver...");
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            System.out.println("OK!");
            return true;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Intenta conectar con la base de datos.
     *
     * @return true si pudo conectarse, false en caso contrario
     */
    public static boolean connect() {
        try {
        	conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	System.out.println("Base de dades conectada");
        	return true;
        } catch (SQLException e) {
        	throw new IllegalStateException("No s'ha pogut connectar a la base de dades", e);
        }
    }

    /**
     * Comprueba la conexión y muestra su estado por pantalla
     *
     * @return true si la conexión existe y es válida, false en caso contrario
     * @throws SQLException 
     */
    public static boolean isConnected() throws SQLException {
        // Comprobamos estado de la conexión
    	try {
    		if (conn.isClosed()) {
    			System.out.println(DB_MSQ_CONN_NO);
            	return false;
            } else {
            	System.out.println(DB_MSQ_CONN_OK);
            	return true;
            }
    	} catch (SQLException ex) {
    		System.out.println(DB_MSQ_CONN_NO);
    		return false;
    	}
    }

    /**
     * Cierra la conexión con la base de datos
     * @throws SQLException 
     */
    public static void close() throws SQLException {
    	try {
    		System.out.println("Tancant conexio");
    		conn.close();
    		System.out.println("Conexió tancada");
    	} catch (SQLException ex) {
    		ex.printStackTrace();
    		System.out.println(DB_MSQ_CONN_NO);
    	}
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE TABLA CLIENTES
    //////////////////////////////////////////////////
    ;
    
    // Devuelve 
    // Los argumentos indican el tipo de ResultSet deseado
    /**
     * Obtiene toda la tabla clientes de la base de datos
     * @param resultSetType Tipo de ResultSet
     * @param resultSetConcurrency Concurrencia del ResultSet
     * @return ResultSet (del tipo indicado) con la tabla, null en caso de error
     * @throws DAOException 
     */
    public static ResultSet getTablaClientes(int resultSetType, int resultSetConcurrency) throws DAOException {
        try {
        	// Fem la conexio a la base de dades
        	Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	CallableStatement sentSQL = conn.prepareCall("CALL getClients()", resultSetType, resultSetConcurrency);
        	ResultSet rs = sentSQL.executeQuery();
        	return rs;
        } catch (SQLException ex) {
        	throw new DAOException(ex);
        }

    }

    /**
     * Obtiene toda la tabla clientes de la base de datos
     *
     * @return ResultSet (por defecto) con la tabla, null en caso de error
     * @throws SQLException 0
     */
    public static ResultSet getTablaClientes() throws DAOException, SQLException {
    	// Indiquem que llegeixi cap endavant(per defecte) i només es de lectura
    	return getTablaClientes(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
    }

    /**
     * Imprime por pantalla el contenido de la tabla clientes
     * @throws SQLException 
     * @throws DAOException 
     */
    public void printTablaClientes() throws SQLException, DAOException {
    	try {
    		// Obtenim el resultat
    		ResultSet resultat = ClientJDBCDAO.getTablaClientes();
    		// Recorrem el resultat
    		while (resultat.next()) {
    		    // Agafem els camps
    			int id = resultat.getInt(DB_CLI_ID);
    			String nom = resultat.getString(DB_CLI_NOM);
    			String direccio = resultat.getString(DB_CLI_DIR);
    			System.out.println("Client amb id = " + id + " nom = " + nom + " direccio = " + direccio);
    		}
    	} catch (SQLException ex) {
    		throw new DAOException(ex);
    	}
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE UN SOLO CLIENTE
    //////////////////////////////////////////////////
    ;
    
    /**
     * Solicita a la BD el cliente con id indicado
     * @param id id del cliente
     * @return ResultSet con el resultado de la consulta, null en caso de error
     * @throws DAOException 
     */
    public static ResultSet getCliente(int id) throws DAOException {
        try {
        	// Fem la conexio a la base de dades
        	Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	/*
        	Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        	String consulta = "SELECT * FROM clientes WHERE id = '" + id + "';"; 
        	ResultSet resultat = stm.executeQuery(consulta);
        	*/
        	CallableStatement stm = connection.prepareCall("CALL getClientById(?)", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        	stm.setLong(1, id);
        	ResultSet resultat = stm.executeQuery();
        	resultat.next();
        	return resultat;
        } catch(Exception ex) {
        	throw new DAOException(ex);
        }
    }

    /**
     * Comprueba si en la BD existe el cliente con id indicado
     *
     * @param id id del cliente
     * @return verdadero si existe, false en caso contrario
     * @throws DAOException 
     */
    public boolean existsCliente(int id) throws DAOException {
        try {
        	// Obtenim el client
        	ResultSet resultat = getCliente(id);
        	// Si es null, no existeix, en cas contrari, true
        	if (resultat == null) {
        		return false;
        	}
        	// Tanquem el resultat
        	resultat.close();
        	return true;
        }
        catch (SQLException ex) {
            throw new DAOException(ex);
        }
    }

    /**
     * Imprime los datos del cliente con id indicado  --> Carga objecto Client
     *
     * @param id id del cliente
     * @throws DAOException 
     */
    public static void printCliente(int id) throws DAOException {
        try {
        	// Obtenim el client
        	ResultSet resultat = getCliente(id);
        	// Si no existeix llancem excepcio
        	if (resultat == null) {
        		throw new DAOException("Client amb id " + id +  " no existeix");
        	}
        	// En cas contrari mostrem l'informacio del client
        	int clientId = resultat.getInt(DB_CLI_ID);
        	String clientNom = resultat.getString(DB_CLI_NOM);
        	String clientDireccio = resultat.getString(DB_CLI_DIR);
        	System.out.println("Client amb id = " + clientId + " nom = " + clientNom + " direccio = " + clientDireccio);
        } catch (SQLException ex) {
        	throw new DAOException(ex);
        }
    }

    /**
     * Solicita a la BD insertar un nuevo registro cliente
     *
     * @param nombre nombre del cliente
     * @param direccion dirección del cliente
     * @return verdadero si pudo insertarlo, false en caso contrario
     * @throws DAOException 
     */
    public boolean insertCliente(String nombre, String direccion) throws DAOException {
        try {
        	// Obtenim tots els clients
        	// Permitim l'actualitzacio
        	ResultSet resultat = getTablaClientes(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        	// Creem un nou registre i l'insertem
        	resultat.moveToInsertRow();
        	resultat.updateString(2, nombre);
        	resultat.updateString(3, direccion);
        	resultat.insertRow();
        	System.out.println("Client afegit correctament");
        	return true;
        } catch (SQLException ex) {
        	throw new DAOException("No s'ha pogut afegir el client");
        }
    }

    /**
     * Solicita a la BD modificar los datos de un cliente
     *
     * @param id id del cliente a modificar
     * @param nombre nuevo nombre del cliente
     * @param direccion nueva dirección del cliente
     * @return verdadero si pudo modificarlo, false en caso contrario
     * @throws DAOException 
     */
    public boolean updateCliente(int id, String nuevoNombre, String nuevaDireccion) throws DAOException {
    	try {
        	// Obtenim el client
        	ResultSet resultat = getCliente(id); 
        	// Si no existeix llancem una excepcio
        	if (!existsCliente(id)) {
        		throw new DAOException("El client seleccionat no existeix");
        	}
        	// Modifiquem els camps
        	resultat.updateString(2, nuevoNombre);
        	resultat.updateString(3, nuevaDireccion);
        	resultat.updateRow();
        	resultat.close();
        	System.out.println("Client modificat correctament");
        	return true;
        } catch (SQLException ex) {
        	throw new DAOException("No s'ha pogut modificar el client");
        }
    }

    /**
     * Solicita a la BD eliminar un cliente
     *
     * @param id id del cliente a eliminar
     * @return verdadero si pudo eliminarlo, false en caso contrario
     * @throws DAOException 
     */
    public boolean deleteCliente(int id) throws DAOException {
    	try {
        	// Obtenim el client
        	ResultSet resultat = getCliente(id); 
        	// Si no existeix llancem una excepcio
        	if (!existsCliente(id)) {
        		throw new DAOException("El client seleccionat no existeix");
        	}
        	// borrem el registre
        	resultat.deleteRow();
        	resultat.close();
        	System.out.println("Client borrat");
        	return true;
        } catch (SQLException ex) {
        	throw new DAOException(ex);
        }
    }

}
