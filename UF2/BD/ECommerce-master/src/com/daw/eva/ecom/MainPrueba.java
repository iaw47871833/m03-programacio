package com.daw.eva.ecom;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class MainPrueba {

	public static void main(String[] args) throws SQLException, DAOException {
		
		/*
		ResultSet resultat = ClientJDBCDAO.getTablaClientes();
		ResultSetMetaData rsmd = resultat.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		while (resultat.next()) {
		    for (int i = 1; i <= columnsNumber; i++) {
		        if (i > 1) System.out.print(",  ");
		        String columnValue = resultat.getString(i);
		        System.out.print(columnValue + " " + rsmd.getColumnName(i));
		    }
		    System.out.println("");
		}
		*/
		ProductJDBCDAO productDAO = new ProductJDBCDAO();
		ResultSet resultat = ClientJDBCDAO.getCliente(2);
		int id = resultat.getInt("id");
		String nom = resultat.getString("nombre");
		String direccio = resultat.getString("direccion");
		System.out.println("Client amb id = " + id + ", nom = " + nom + ", direccio = " + direccio);
		System.out.println("--------------");
		ClientJDBCDAO.printCliente(6);
		System.out.println("--------------");
		productDAO.printTablaProductos();
		ResultSet productResult = productDAO.getProductById(3);
		String nombree = productResult.getString("nombre");
		String descripcio = productResult.getString("descripcion");
		double preu = productResult.getFloat("precio");
		System.out.println(" nom = " + nombree + ", descripcio = " + descripcio + ", preu = " + preu);
		ComandaJDBCDAO comandaDao = new ComandaJDBCDAO();
		comandaDao.printTablaComanda();
	}

}
