package com.daw.eva.ecom.business.entities;

import java.sql.Timestamp;

public class Comanda {
	private int idCliente;
	private int idProducto;
	private int cantidad;
	private double precio;
	private Timestamp fechaComanda;
	/**
	 * @param idCliente
	 * @param idProducto
	 * @param cantidad
	 * @param precio
	 * @param fechaComanda
	 */
	public Comanda(int idCliente, int idProducto, int cantidad, double precio, Timestamp fechaComanda) {
		this.idCliente = idCliente;
		this.idProducto = idProducto;
		this.cantidad = cantidad;
		this.precio = precio;
		this.fechaComanda = fechaComanda;
	}
	/**
	 * @return the idCliente
	 */
	public int getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	/**
	 * @return the idProducto
	 */
	public int getIdProducto() {
		return idProducto;
	}
	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}
	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	/**
	 * @return the precio
	 */
	public double getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	/**
	 * @return the fechaComanda
	 */
	public Timestamp getFechaComanda() {
		return fechaComanda;
	}
	/**
	 * @param fechaComanda the fechaComanda to set
	 */
	public void setFechaComanda(Timestamp fechaComanda) {
		this.fechaComanda = fechaComanda;
	}
	
	@Override
	public String toString() {
		return "Comanda [idCliente=" + idCliente + ", idProducto=" + idProducto + ", cantidad=" + cantidad + ", precio="
				+ precio + ", fechaComanda=" + fechaComanda + "]";
	}
	
	
	
}
