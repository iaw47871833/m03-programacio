package com.daw.eva.ecom;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class ComandaJDBCDAO implements ComandaDAO {

	// Conexión a la base de datos
    private static Connection conn = null;

    // Configuración de la conexión a la base de datos
    private static final String DB_HOST = "localhost";
    private static final String DB_PORT = "3307";
    private static final String DB_NAME = "tienda";
    private static final String DB_URL = "jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "?serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "password";
    private static final String DB_MSQ_CONN_OK = "CONEXIÓN CORRECTA";
    private static final String DB_MSQ_CONN_NO = "ERROR EN LA CONEXIÓN";

    // Configuración de la tabla Clientes
    private static final String DB_CLI = "productos";
    private static final String DB_CLI_SELECT = "SELECT * FROM " + DB_CLI;
    private static final String DB_CLI_ID = "id";
    private static final String DB_CLI_NOM = "nombre";
    private static final String DB_CLI_DESC = "descripcion";
    private static final String DB_CLI_PREC = "precio";
    private static Connection connection = null;

    //////////////////////////////////////////////////
    // MÉTODOS DE CONEXIÓN A LA BASE DE DATOS
    //////////////////////////////////////////////////
    ;
    
    /**
     * Intenta cargar el JDBC driver.
     * @return true si pudo cargar el driver, false en caso contrario
     */
    public static boolean loadDriver() {
        try {
            System.out.print("Loading Driver...");
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            System.out.println("OK!");
            return true;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Intenta conectar con la base de datos.
     *
     * @return true si pudo conectarse, false en caso contrario
     */
    public static boolean connect() {
        try {
        	conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	System.out.println("Base de dades conectada");
        	return true;
        } catch (SQLException e) {
        	throw new IllegalStateException("No s'ha pogut connectar a la base de dades", e);
        }
    }

    /**
     * Comprueba la conexión y muestra su estado por pantalla
     *
     * @return true si la conexión existe y es válida, false en caso contrario
     * @throws SQLException 
     */
    public static boolean isConnected() throws SQLException {
        // Comprobamos estado de la conexión
    	try {
    		if (conn.isClosed()) {
    			System.out.println(DB_MSQ_CONN_NO);
            	return false;
            } else {
            	System.out.println(DB_MSQ_CONN_OK);
            	return true;
            }
    	} catch (SQLException ex) {
    		System.out.println(DB_MSQ_CONN_NO);
    		return false;
    	}
    }

    /**
     * Cierra la conexión con la base de datos
     * @throws SQLException 
     */
    public static void close() throws SQLException {
    	try {
    		System.out.println("Tancant conexio");
    		conn.close();
    		System.out.println("Conexió tancada");
    	} catch (SQLException ex) {
    		ex.printStackTrace();
    		System.out.println(DB_MSQ_CONN_NO);
    	}
    }
	
    public static ResultSet getTablaComanda(int resultSetType, int resultSetConcurrency) throws DAOException {
        try {
        	// Fem la conexio a la base de dades
        	Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	String consulta = "SELECT c.nombre, c.direccion, p.nombre, cd.cantidad, cd.precioTotal, cd.fechaComanda\n"
        			+ "From comanda cd\n"
        			+ "inner join clientes c\n"
        			+ "on cd.idCliente = c.id\n"
        			+ "inner join productos p\n"
        			+ "on cd.idProducto = p.id;";
        	PreparedStatement sentSQL = conn.prepareStatement(consulta);
        	ResultSet rs = sentSQL.executeQuery();
        	return rs;
        } catch (SQLException ex) {
        	throw new DAOException(ex);
        }

    }
    
	public void printTablaComanda() throws DAOException {
		try {
    		// Obtenim el resultat
    		ResultSet resultat = ComandaJDBCDAO.getTablaComanda(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
    		// Recorrem el resultat
    		while (resultat.next()) {
    		    // Agafem els camps
    			String nomClient = resultat.getString("c.nombre");
    			String direccio = resultat.getString("c.direccion");
    			String nomProducte = resultat.getString("p.nombre");
    			int quantitat = resultat.getInt("cd.cantidad");
    			double preuTotal = resultat.getDouble("cd.precioTotal");
    			Timestamp data = resultat.getTimestamp("cd.fechaComanda");
    			System.out.println("Nom client: " + nomClient + ", direccio: " + direccio);
    			System.out.println("Producte: " + nomProducte + ", quantitat: " + quantitat + ", preu total: " + preuTotal + ", data: " + data);
    		}
    	} catch (SQLException ex) {
    		throw new DAOException(ex);
    	}
		
	}

}
