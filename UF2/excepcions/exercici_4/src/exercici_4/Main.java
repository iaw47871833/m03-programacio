package exercici_4;

public class Main {

	public static void main(String[] args) {
		try {
			Persona p = new Persona(18);
			System.out.println("Edat actual: " + p.getEdad());
			p.setEdat(21);
			System.out.println("Edat actual: " + p.getEdad());
			p.setEdat(-24);
			p.setEdat(40);
			System.out.println("Edat actual: " + p.getEdad());
		} catch(IllegalArgumentException e) {
			System.out.println(e);
		}
	}

}
