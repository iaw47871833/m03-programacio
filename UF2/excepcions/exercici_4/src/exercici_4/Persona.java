package exercici_4;

public class Persona {
	private int edat;

	public Persona(int edat) {
		this.edat = edat;
	}

	public void setEdat(int edat) throws IllegalArgumentException {
		try {
			if (edat < 0) {
				System.out.println("L'edat no pot ser negativa");
				throw new IllegalArgumentException();
			} else {
				System.out.println("Edat modificada");
				this.edat = edat;
			}
		} catch (IllegalArgumentException e) {
			System.out.println(e);
		}
	}

	public int getEdad() {
		return this.edat;
	}
}
