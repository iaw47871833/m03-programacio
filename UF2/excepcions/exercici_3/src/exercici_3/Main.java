package exercici_3;

public class Main {

	public static void main(String[] args) {
		try {
			// Guardem el resultat
			divisio(12, 2);
			divisio(3, 1);
			divisio(4, 0);
		} catch (Exception e) {
			System.out.println("No es pot dividir entre 0");
		}
	}
	
	/**
	 * divisio
	 * 
	 * Dividir 2 numeros
	 * 
	 * @param num1
	 * @param num2
	 * @return divisio dels numeros
	 */
	public static double divisio(int num1, int num2) throws Exception {
		// Si el segon numero es igual a 0 creem l'excepció
		if (num2 == 0) {
			throw new Exception();
		} else {
			double resultat = num1 / num2;
			System.out.println("El resultat de " + num1 + " entre " + num2 + " és: " + resultat);
			return resultat;
		}
	}

}
