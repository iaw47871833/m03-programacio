package exercici_2;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		//FileOutputStream f = new FileOutputStream("../docs/test.txt");
		// No apareix SecurityException perqué no existeix cap mena de securitat ni
		// un métode que denega l'accés a l'arxiu
		// Intentem executar la funció
		try {
			obrirTancarArxiu();
		} catch(FileNotFoundException f) {
			System.out.println("error filenotfound");
			System.out.println(f.toString());
		} catch(IOException e) {
			System.out.println("error ");
			System.out.println(e.toString());
		}
	}

	/**
	 * obrirTancarArxiu
	 * 
	 * obrir un arxiu i seguidament tancar-lo
	 */
	public static void obrirTancarArxiu() throws FileNotFoundException, IOException {
		FileOutputStream f = new FileOutputStream("../docs/test.txt");
		f.close();
	}
}
