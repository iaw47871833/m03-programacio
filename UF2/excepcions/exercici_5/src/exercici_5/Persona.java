package exercici_5;

public class Persona {
	private int edat;

	public Persona(int edat) {
		this.edat = edat;
	}

	public void setEdat(int edat) throws Exercici5ValidarEdatException {
		try {
			if (edat < 0) {
				throw new Exercici5ValidarEdatException(edat);
			} else if(edat > 100) {
				throw new Exercici5ValidarEdatException(edat);
			} else {
				this.edat = edat;
			}
		} catch (Exercici5ValidarEdatException e) {
			System.out.println(e);
		}
	}

	public int getEdat() {
		return this.edat;
	}
}
