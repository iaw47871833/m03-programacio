package exercici_5;

public class Exercici5ValidarEdatException extends Exception {
	public Exercici5ValidarEdatException(int edat) {
		if (edat < 0) {
			System.out.println("L'edat no pot ser negativa");
		} else if (edat > 100) {
			System.out.println("L'edat no pot ser major de 100");
		}
	}
}
