package exercici_5;

public class Main {

	public static void main(String[] args) {
		try {
			Persona p = new Persona(25);
			System.out.println("Edat actual: " + p.getEdat());
			p.setEdat(21);
			System.out.println("Edat actual: " + p.getEdat());
			p.setEdat(-24);
			p.setEdat(104);
		} catch (Exercici5ValidarEdatException e) {
			System.out.println(e.toString());
		}
	}

}
