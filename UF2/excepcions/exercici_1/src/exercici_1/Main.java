package exercici_1;

public class Main {

	public static void main(String[] args) {
		// Codi que genera un error de tipus ArrayIndexOutOfBounds
		// int numeros[] = {1, 2, 3};
		// System.out.println(numeros[5]);
		// Introduim codi després de l'error
		// System.out.println("Final del programa!"); // No s'executarà perque hi ha un
		// error abans
		try {
			funcio1();
			String cadena = "hola que tal";
			char caracter = cadena.charAt(50);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
			System.out.println("Codi del catch ArrayIndexOutOfBoundsException");
		} catch (StringIndexOutOfBoundsException e) {
			System.out.println("Codi del catch StringIndexOutOfBounds");
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		} catch(Exception e) {
			System.out.println("Codi del catch Exception");
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		} finally {
			System.out.println("Codi del finally");
		}
		// El bloc de finally es mostrara si surt o no un error.
		// Si probem de posar una excepcio StringIndexOutOfBoundsException
		// s'executara el codi del catch que fa referencia a aquesta excepció.
		// Si probem amb Exception, mostra el missatge del catch de StringIndexOutOfBoundsException
		// perquè aquesta excepció hereta de Exception

		System.out.println("Final del programa!");
	}

	public static void funcio1() {
		System.out.println("Funcio 2");
		funcio2();
		System.out.println("Fi de la funcio 2");
		/*
		System.out.println("Funcio 3");
		funcio3();
		System.out.println("Fi de la funcio 3");
		*/
	}

	public static void funcio2() {
		//System.out.println("Codi de la funcio 2");
		try {
			funcio3();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void funcio3() {
		int numeros[] = { 1, 2, 3 };
		System.out.println(numeros[5]);
	}

}
