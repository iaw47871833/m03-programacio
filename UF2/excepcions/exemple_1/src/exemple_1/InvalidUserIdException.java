package exemple_1;

public class InvalidUserIdException extends Exception {
	@Override
	public String toString() {
		return "Invalid user id entered";
	}
}
