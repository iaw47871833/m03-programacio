package exemple_1;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix el user id:");
		int user_id = sc.nextInt();
		
		try {
			if (user_id != 1234) {
				throw new InvalidUserIdException();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		sc.close();
	}

}


