package exemple_2;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		int current_balance = 1000;
		System.out.println("---- Entra l'ingrès a dispositar ----");
		Scanner sc = new Scanner(System.in);
		int deposit_amount = sc.nextInt();
		try {
			if (deposit_amount < 0) {
				throw new NegativeNotAllowedException();
			} else {
				current_balance += deposit_amount;
				System.out.println("Updated balance is: " + current_balance);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
