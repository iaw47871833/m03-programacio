package examen;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;

public class Exercici1 {

	public static void main(String[] args) throws IOException {
		// Comprobem que existeix el directori patata dins del directori
		// exercici1
		String pathPatata = "./fitxers_examen/exercici1/patata";
		File directoriPatata = new File(pathPatata);
		// En el cas de que existeixi la borrem
		if (directoriPatata.exists()) {
			FileUtils.deleteDirectory(directoriPatata);
		}
		FileUtils.forceMkdir(directoriPatata);
		// Creem el fitxer index.txt dins de patata
		String indexPath = pathPatata + "/index.txt";
		File indexFile = new File(indexPath);
		indexFile.createNewFile();
		// Escrivim 2 linies de text dins de index.txt
		FileUtils.write(indexFile, "linea 1 d'index.txt\n");
		FileUtils.write(indexFile, "linea 2 d'index.txt", true);
		// Copiem el el contingut del directori dirACopiar dins del directori patata
		File directoriCopiar = new File("./fitxers_examen/dirACopiar");
		FileUtils.copyDirectory(directoriCopiar, directoriPatata);
		// Copia el fitxer index.txt en el directori fitxers_examen
		String pathFitxersExamen = "./fitxers_examen";
		File directoriFitxersExamen = new File(pathFitxersExamen);
		FileUtils.copyFileToDirectory(indexFile, directoriFitxersExamen);
		// Rastreja tots els directoris i fitxers que hi ha dins del directori patata
		Iterator it = FileUtils.iterateFilesAndDirs(directoriPatata, new WildcardFileFilter("*.txt"),
				new WildcardFileFilter("*"));
		while (it.hasNext()) {
			File fileTmp = (File) it.next();
			System.out.print("NOM DEL FILE TROBAT: " + ((File) fileTmp).getName());
			if (((File) fileTmp).isFile()) {
				System.out.println(" I ES UN ARXIU");
			} else {
				System.out.println(" I ES UN DIRECTORI");
			}

		}
		/*
		 * Per cada directori i fitxer trobat, comproba els seus permisos de lectura,
		 * escriptura i execució, si és ocult i el seu tamany. Envia el seu nom i les
		 * dades anteriors a la funció copiarNomEnFitxer() que serà l'encarregada de
		 * escriure aquestes dades en el fitxer index.txt (per escriure fes servir els
		 * Writers). Aquest codi està donat a l’annexe de la prova. Només has de crear
		 * el mètode copiarNomEnFitxer(). Es dona a l’annex la signatura del mètode.
		 */
		FileWriter fw = null;
		PrintWriter pw = null;
		StringBuilder permisosFile = new StringBuilder("---, ");
		File fitxerIndex2;
		
		boolean append = true;
		fw = new FileWriter(indexFile, append);
		pw = new PrintWriter(fw, true);
		Iterator it2 = FileUtils.iterateFilesAndDirs(directoriPatata, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		// Segons l'ajuda el TrueFileFilter.INSTANCE determina tots els fitxers i directoris.
		while (it2.hasNext()) {
		File fileTmp = (File) it2.next();
		System.out.println(fileTmp.getName());
		if (fileTmp.canRead())
		permisosFile.setCharAt(0, 'r');
		if (fileTmp.canWrite())
		permisosFile.setCharAt(1, 'w');
		if (fileTmp.canExecute())
		permisosFile.setCharAt(2, 'x');
		if (fileTmp.isHidden()){
		permisosFile.append("OCULT, ");
		}
		permisosFile.append("tamany = ");
		permisosFile.append(FileUtils.sizeOf(fileTmp));
		permisosFile.append(" bytes.");
		if (fileTmp.isDirectory()) {
		copiarNomEnFitxer(fileTmp.getName(), 0,
		permisosFile, pw);
		} else {
		copiarNomEnFitxer(fileTmp.getName(), 1,
		permisosFile, pw);
		}
		permisosFile = new StringBuilder("---, ");
		}
		
		// Netejem el directori patata
		FileUtils.cleanDirectory(directoriPatata);
		// Copiem el fitxer index.txt al directori patata
		String indexPath2 = "./fitxers_examen/index.txt";
		File indexFile2 = new File(indexPath2);
		FileUtils.copyFileToDirectory(indexFile2, directoriPatata);
		// Comprobem que el contingut dels 2 fitxers index.txt es el mateix
		if (FileUtils.contentEquals(indexFile, indexFile2)) {
			System.out.println("El contingut dels 2 fitxers SI és el mateix");
		} else {
			System.out.println("El contingut dels 2 fitxers NO és el mateix");
		}
	}
	
	private static void copiarNomEnFitxer(String nomFileTrobat, int tipusFile, StringBuilder permisosFile, PrintWriter pw) {
		// permisos de lectura, escriptura i execució, si és ocult i el seu tamany.
		if (tipusFile == 0) {
			pw.println("Nom del directori: " + nomFileTrobat + ", permisos: " + permisosFile);
		} else {
			pw.println("Nom del arxiu: " + nomFileTrobat + ", permisos: " + permisosFile);
		}
		// NO ACABAT
	}

}
