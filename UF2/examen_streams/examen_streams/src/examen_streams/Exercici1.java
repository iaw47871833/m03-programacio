package examen_streams;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class Exercici1 {

	public static void main(String[] args) throws IOException {
		// Comprobem que existeix el directori patata dins del directori
		// exercici1
		String pathPatata = "./fitxers_examen/exercici1/patata";
		File directoriPatata = new File(pathPatata);
		// En el cas de que existeixi la borrem
		if (directoriPatata.exists()) {
			FileUtils.deleteDirectory(directoriPatata);
		}
		FileUtils.forceMkdir(directoriPatata);
		// Creem el fitxer index.txt dins de patata
		String indexPath = pathPatata + "/index.txt";
		File indexFile = new File(indexPath);
		indexFile.createNewFile();
		// Escrivim 2 linies de text dins de index.txt
		FileUtils.write(indexFile, "linea 1 d'index.txt\n");
		FileUtils.write(indexFile, "linea 2 d'index.txt", true);
		// Copiem el el contingut del directori dirACopiar dins del directori patata
		File directoriCopiar = new File("./fitxers_examen/dirACopiar");
		//String[] extensions = { "txt", "java" };
		//List<File> llistaArxius = (List<File>) (FileUtils.listFiles(directoriCopiar, extensions, true));
		// Recorrem els arxius del directori
		/*for (File arxiu : llistaArxius) {
			System.out.println(arxiu.getName());
		}*/
		//FileUtils.copyFileToDirectory(srcFile, destDir);
	}

}
