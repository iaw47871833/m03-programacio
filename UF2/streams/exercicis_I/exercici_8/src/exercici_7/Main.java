package exercici_7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

	public static void main(String[] args) throws IOException {
		// Guardem l'URL
		String link = "https://elpais.com/";
		guardarLinks(link);
		guardarContingut(link);
	}

	public static void guardarLinks(String link) throws IOException {
		// Inicialitzem el fitxer a escriure i els printer
		FileWriter fw = null;
		PrintStream ps = null;
		// Buffer per llegir lineas
		BufferedReader br = null;
		try {
			// Creem l'objecte URL
			URL miUrl = new URL(link);
			InputStreamReader in = new InputStreamReader(miUrl.openStream());
			br = new BufferedReader(in);
			// Indiquem l'arxiu on escriurem els links
			ps = new PrintStream(new File("links.txt"));
			fw = new FileWriter("links.txt");
			// Patro a buscar
			Pattern patro = Pattern.compile("(<a href=\\\")([^\\\"]+)(\\\")");
			// Declarem la linea
			String linea = "";
			// Recorrem les lineas de la URL
			while ((linea = br.readLine()) != null) {
				Matcher matcher = patro.matcher(linea);
				while (matcher.find()) {
					// Escribim el segon grup del match al fitxer
					fw.write(matcher.group(2) + "\n");
				}
			}
			System.out.println("Links guardats");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
		// Tanquem els arxius
		fw.close();
		br.close();
		ps.close();

	}

	public static void guardarContingut(String link) throws IOException {
		// Inicialitzem el fitxer a escriure i els printer
		FileWriter fw = null;
		PrintStream ps = null;
		// Buffer per llegir lineas
		BufferedReader br = null;
		try {
			// Creem l'objecte URL
			URL miUrl = new URL(link);
			InputStreamReader in = new InputStreamReader(miUrl.openStream());
			br = new BufferedReader(in);
			// Indiquem l'arxiu on escriurem els links
			ps = new PrintStream(new File("contingut.txt"));
			fw = new FileWriter("contingut.txt");
			// Patro a buscar
			Pattern patro = Pattern.compile("(<p)([^>]*)(>)([^<]*?)(</p>)");
			// Declarem la linea
			String linea = "";
			// Declarem el text que conte la linea
			String text = "";
			// Recorrem les lineas de la URL
			while ((linea = br.readLine()) != null) {
				text = text + linea;
			}
			Matcher matcher = patro.matcher(text);
			while (matcher.find()) {
				// Guardem el 4t grup de la regex
				fw.write(matcher.group(4) + "\n");
			}
			System.out.println("Contingut guardats");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
		// Tanquem els arxius
		fw.close();
		br.close();
		ps.close();
	}

}
