package exercici_9;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// Guardem el path del directori
		String path = "../../..";
		// Inicialitzem el directori
		File directori = new File(path);
		// ArrayList per guardar el nom dels arxius i directoris
		List<String> llista = new ArrayList<String>();
		mostrarFitxersDirectoris(directori, llista);
		// Mostrem la llista
		System.out.println(llista);
	}

	public static void mostrarFitxersDirectoris(File directori, List<String> llista) {
		// Guardem el path
		String path = directori.getPath();
		// Llista dels arxius
		String[] arxius = directori.list();
		// Recorrem la llista d'arxius
		for (int i = 0; i < arxius.length; i++) {
			// Iniciem el fitxer
			File f = new File(path + "/" + arxius[i]);
			// Si es un directori tornem a cridar el metode
			// En cas contrari el fiquem a la llista
			if (f.isDirectory()) {
				mostrarFitxersDirectoris(f, llista);
			} else {
				llista.add(arxius[i]);
			}
		}
	}
	
}
