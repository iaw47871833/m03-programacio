package exercici_3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/*
 Escriu un programa que compari dos arxius de text línia per línia. 
S'ha de llegir una línia de cada arxiu i comparar-les. Si aquestes no coincideixen, 
s'ha de mostrar  el número de línia que falla i les dues línies que són diferents. 
El programa processarà totes les línies de l'arxiu. 
 */

public class Main {

	public static void main(String[] args) throws IOException {
		// Inicialitzem els fitxers
		File fitxer1 = new File("fitxers/text1.txt");
		File fitxer2 = new File("fitxers/text2.txt");
		// Inicialitzem el file reader
		FileReader fr1 = null;
		FileReader fr2 = null;
		// Inicialitzem el buffer
		BufferedReader br1;
		BufferedReader br2;
		// Linies dels arxius
		String lineaf1;
		String lineaf2;
		
		try {
			fr1 = new FileReader(fitxer1); // Inicialitzem l'entrada del fitxer
			fr2 = new FileReader(fitxer2);
			br1 = new BufferedReader(fr1); // Inicialitzem el buffer amb l'entrada
			br2 = new BufferedReader(fr2);
			lineaf1 = br1.readLine();
			lineaf2 = br2.readLine();
			// Contador per saber quina linea hi ha conflicte
			int cont = 1;
			// Mentre hi hagi lineas, llegir la linea i comprobem que aquestes son iguals
			// En el moment que alguna de les lines sigui diferent, sortirem del bucle
			while(lineaf1 != null && lineaf2 != null && lineaf1.equals(lineaf2)) {
				lineaf1 = br1.readLine();
				lineaf2 = br2.readLine();
				cont++;
			}
			// Comprobem que no hi hagi linies diferents
			if (lineaf1 != null || lineaf2 != null) {
				System.out.println("Linea que falla: " + cont);
				System.out.println("Linea arxiu 1: " + lineaf1);
				System.out.println("Linea arxiu 2: " + lineaf2);
			}
		} catch(FileNotFoundException e) {
			System.out.println("Fitxer no existeix");
		} catch(IOException e) {
			System.out.println(e.getMessage());
		} finally {
			if (fr1 != null) fr1.close();
			if (fr2 != null) fr2.close();
		}
	}

}
