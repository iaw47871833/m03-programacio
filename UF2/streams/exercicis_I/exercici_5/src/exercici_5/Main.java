package exercici_5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		String directoriStr = ".";
		try {
			File directori = new File(directoriStr);
			if (directori.exists() && directori.isDirectory()) {
				// Llistem els arxius
				File[] llistaArxius = directori.listFiles();
				comptarLineasCaracters(llistaArxius);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void comptarLineasCaracters(File[] llistaArxius) throws IOException {
		// Recorrem els arxius
		for (File arxiu: llistaArxius) {
			// Inicialitzem el Reader i el buffer
			FileReader fr = null;
			BufferedReader br;
			String linea;
			int contLineas = 0;
			int numCaracters = 0;
			// Si es un arxiu, contem les lineas i 
			if (arxiu.isFile()) {
				try {
					fr = new FileReader(arxiu);
					br = new BufferedReader(fr);
					while((linea = br.readLine()) != null) {
						contLineas++;
						numCaracters += linea.length();
					}
					System.out.println("L'arxiu " + arxiu.getName() + " té " + contLineas + " lineas i " + numCaracters + " caracters");
				} catch (FileNotFoundException e) {
					System.out.println("Fitxer " + arxiu.getName() + " no existeix");
				} catch (IOException e) {
					System.out.println(e.getMessage());
				} finally {
					if (fr != null) {
						fr.close();
					}
				}
			}
			
		}
	}
}
