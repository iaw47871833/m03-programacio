package exercici_4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		// Guardem el directori actual
		String nomDirectoriActual = "./src";
		try {
			File directoriActual = new File(nomDirectoriActual);
			// Comprobem que existeix i es un directori
			if (directoriActual.exists() && directoriActual.isDirectory()) {
				// Llistem els arxius
				File[] llistaArxius = directoriActual.listFiles();
				for (int i = 0; i < llistaArxius.length; i++) {
					if (llistaArxius[i].isFile()) {
						System.out.println(llistaArxius[i].getName());
					}
				}
				// Creem el directori
				String directoriNou = crearDirectori();
				// Copiem els arxius
				copiarArxius(directoriNou, llistaArxius);
			}
		} catch (Exception e) {
			System.out.println("No s'ha realitzat la copia de seguretat");
		}
	}
	
	public static String crearDirectori() {
		// boolean que ens servira per saber si s'ha creat el directori
		boolean directoriCreat = false;
		// Numero maxim de backups
		int maxim = 5;
		// Inicialitzem el nom del directori backup
		String nomDirectoriBackup = "";
		for (int i = 0; i < maxim && !directoriCreat; i++) {
			// Nom del directori backup
			nomDirectoriBackup = "backUp" + i;
			// Comprobem que existeix
			File directoriBackup = new File(nomDirectoriBackup);
			if (!directoriBackup.exists()) {
				System.out.println("es pot crear");
				// Creem el directori
				directoriBackup.mkdir();
				directoriCreat = true;
				System.out.println("directori " + directoriBackup.getName() + " creat");
			}
		}
		return nomDirectoriBackup;
	}
	
	public static void copiarArxius(String directoriCopia, File[] arxius) {
		// Recorrem els arxius
		for (File arxiu: arxius) {
			// Comprobem que es un arxiu
			if (arxiu.isFile()) {
				// Creem l'arxiu copia
				File arxiuCopia = new File(directoriCopia + "/" + arxiu.getName());
				try {
					if (arxiu.canRead()) {
						arxiuCopia.createNewFile();
						// El copiem 
						copiar(arxiu, arxiuCopia);
					} else {
						System.out.println("L'arxiu " + arxiuCopia.getName() + " no te permisos de lectura");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void copiar(File fileInput, File fileOutput) throws IOException {
		boolean append = true;
		byte buff[] = new byte[100];
		FileInputStream fileInputStream = new FileInputStream(fileInput);
		FileOutputStream fileOutputStream = new FileOutputStream(fileOutput, append);
		try {
			int reads;
			while ((reads = fileInputStream.read(buff, 0, 100))>-1){
				fileOutputStream.write(buff,0,reads);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			fileInputStream.close();
			fileOutputStream.close();
		}
	}

}
