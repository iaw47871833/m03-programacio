
public class ExempleDebugar {

	public static void main(String[] args) {
		int[] myIntArray = {1,2,3,3,4,2};
		System.out.println("Hi ha " + pendents(myIntArray) + " pendents");
	}
	
	public static int pendents (int[] vec) {
		int pendents = 0;
		for (int i = 0; i < vec.length - 1; i++) {
			if (vec[i] < vec[i + 1] || vec[i] > vec[i + 1]) {
				pendents++;
			}
		}
		return pendents;
	}

}
