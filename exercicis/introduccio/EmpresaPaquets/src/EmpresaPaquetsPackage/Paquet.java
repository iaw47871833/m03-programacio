package EmpresaPaquetsPackage;

public class Paquet {
	// Attributes
	/* Package's id */
	private int id;
	/* Package's weight */
	private double weight;
	/* Package's size */
	private double size;
	
	/**
	 * Constructor without parameters
	 * 
	 * @param args Not used
	 */
	public Paquet() {
		super();
	}

	/**
	 * Constructor with parameters
	 * 
	 * @param id
	 * @param weight
	 * @param size
	 */
	public Paquet(int id, double weight, double size) {
		super();
		this.id = id;
		this.weight = weight;
		this.size = size;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the weight
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}

	/**
	 * @return the size
	 */
	public double getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(double size) {
		this.size = size;
	}
	
	
}
