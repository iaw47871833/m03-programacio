package OperacioPackage;

import java.util.Scanner;

public class Opera {

	public static void main(String[] args) {
		// Declaration of Scanner
		Scanner sc = new Scanner(System.in);
		// Input of numbers
		System.out.println("Enter the first value");
		int num1 = sc.nextInt();
		System.out.println("Enter the second value");
		int num2 = sc.nextInt();
		// Operate the given values
		int result = num1 + num2;
		System.out.println("Result = " + result);
		// Close the Scanner
		sc.close();
	}

}
