package PersonaPackage;

public class Estudiant extends Persona {
	// Attributes
	private String course;

	/**
	 * @param age
	 * @param name
	 * @param course
	 */
	public Estudiant(int age, String name, String course) {
		super(age, name);
		this.course = course;
	}
	
	/**
	 * Wave
	 * 
	 * Display the name and age of the person
	 * 
	 * @param args Not used
	 * @return String displaying the name and age of the person
	 */
	@Override
	public String wave() {
		return "Hi, my name is " + super.getName() + ", i'm " + super.getAge() +
				" old and i'm coursing " + this.getCourse();
	}

	/**
	 * @return the course
	 */
	public String getCourse() {
		return course;
	}

	/**
	 * @param course the course to set
	 */
	public void setCourse(String course) {
		this.course = course;
	}
}
