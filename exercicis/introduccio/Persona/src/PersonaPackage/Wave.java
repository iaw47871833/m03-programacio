package PersonaPackage;

public class Wave {

	public static void main(String[] args) {
		// Create objects
		Persona p = new Persona(22, "Alex");
		Professor pf = new Professor(43, "Xavi");
		Estudiant e = new Estudiant(24, "Merce", "DAW");
		// Person's wave
		System.out.println(p.wave());
		System.out.println(pf.wave());
		System.out.println(e.wave());
	}

}
