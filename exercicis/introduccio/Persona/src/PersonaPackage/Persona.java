package PersonaPackage;

/**
 * @author iaw47871833
 *
 */

public class Persona {
	// Attributes
	// Person's age
	private int age;
	// Person's name
	private String name;
	
	public Persona(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}
	
	/**
	 * Wave
	 * 
	 * Display the name and age of the person
	 * 
	 * @param args Not used
	 * @return String displaying the name and age of the person
	 */
	public String wave() {
		return "Hi, my name is " + this.name + " and i'm " + this.age + " old";
	}
	
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Persona [age=" + age + ", name=" + name + "]";
	}
}
