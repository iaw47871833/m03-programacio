package PersonaPackage;

public class Professor extends Persona {

	public Professor(int age, String name) {
		super(age, name);
	}

	/**
	 * Wave
	 * 
	 * Display the name and age of the person
	 * 
	 * @param args Not used
	 * @return String displaying the name and age of the person
	 */
	@Override
	public String wave() {
		return "Hi, my name is " + super.getName() + ", i'm " + super.getAge() + " old and i'm a professor";
	}
}
