package javaCollectionPackage;

import java.util.ArrayList;
import java.util.Iterator;

public class JavaCollections {

	public static void main(String[] args) {
		// Creem l'array list
		ArrayList<String> l1 = new ArrayList<String>();

		// Afegim dos animals a la llista
		l1.add("Gat");
		l1.add("jaguar");

		System.out.println(l1.get(0));
		System.out.println(l1.get(1));

		// Modifiquem l'animal de la posició 0
		l1.set(0, "Gos");

		System.out.println(l1.get(0));
		System.out.println(l1.get(1));

		// Afegim més animals
		l1.add("pantera");
		l1.add("gat");
		l1.add("lynx");

		// Recorrem tota la llista amb un bucle
		for (int i = 0; i < l1.size(); i++) {
			System.out.println("\nAnimal " + i + " : " + l1.get(i));
		}

		// Mirem si l'animal lynx està a la llista
		if (l1.contains("lynx")) {
			System.out.println("Lynx està present a la llista");
		} else {
			System.out.println("Lynx no està present a la llista");
		}

		// For in
		System.out.println("-------For in-------");
		for (String animal : l1) {
			System.out.println(animal);
		}

		// Iterator
		System.out.println("-------Iterator------");
		Iterator i = l1.iterator(); while (i.hasNext() ) {
		System.out.println(i.next()); }
		// Convertim l'arrayList a un array
		System.out.println("-----ArrayList a array------");
		String s[] = new String[l1.size()];
		s = l1.toArray(s);
		// Recorrem l'array
		for (String animal : s) {
			System.out.println(animal);
		}

		// Instanciar cotxes
		System.out.println("------- Cotxes --------");
		Car c1 = new Car(20000, "3series", "BMW");
		Car c2 = new Car(50000, "Tormenta" , "Lamborgini");
		Car c3 = new Car(15000, "Mini" , "Audi");
		ArrayList<Car> c = new ArrayList<Car>();
		
		// Accions del cotxe
		c1.Drive();
		c2.Stop();
		c3.StartEngine();
		
		// Afegim els cotxes
		c.add(c1);
		c.add(c2);
		c.add(c3);
		
		// Recorrem l'arrayList
		System.out.println("Recorrer array list de cotxes");
		for(int o = 0; o < c.size(); o++) {
			System.out.println(c.get(o).brand + " " + c.get(o).name);
		}
		
		for (Car cotxe : c) {
			System.out.println("Cotxe " + cotxe.getName());
		}

	}

}
