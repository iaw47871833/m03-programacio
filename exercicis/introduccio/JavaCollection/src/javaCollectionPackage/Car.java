package javaCollectionPackage;

public class Car implements Vehicle {
	int price;
	String name;
	String brand;
	
	/**
	 * @param price
	 * @param name
	 * @param brand
	 */
	public Car(int price, String name, String brand) {
		this.price = price;
		this.name = name;
		this.brand = brand;
	}
	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}
	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	@Override
	public void Drive() {
		System.out.println("Conduint el cotxe");
		
	}
	@Override
	public void Stop() {
		System.out.println("S'ha parat el cotxe");
		
	}
	@Override
	public void StartEngine() {
		System.out.println("S'esta engegant el cotxe");
		
	}
	
	@Override
	public void TurnLeft() {
		System.out.println("Girant a l'esquerre");
	}
	@Override
	public void TurnRight() {
		System.out.println("Girant a la dreta");
		
	}
	@Override
	public void goITV() {
		System.out.println("Anant a la ITV");
		
	}
}
