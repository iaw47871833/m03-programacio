package programmerPackage;

public class Programmer {
	// Attributes
	// Programmer's id
	private int id;
	// Programmer's name
	private String name;
	// Programmer's salary
	private double salary;
	
	public Programmer() {
		this.id = 1;
		this.name = "john";
		this.salary = 1000;
	}
	
	/**
	 * @param id
	 * @param name
	 * @param salary
	 */
	public Programmer(int id, String name, double salary) {
		this.id = id;
		this.name = name;
		this.salary = salary;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the salary
	 */
	public double getSalary() {
		return salary;
	}
	/**
	 * @param salary the salary to set
	 */
	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Programmer [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}
	
	
}
