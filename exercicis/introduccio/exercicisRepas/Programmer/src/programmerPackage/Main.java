package programmerPackage;

public class Main {

	public static void main(String[] args) {
		// Creation of the Programmer objects
		Programmer p1 = new Programmer();
		Programmer p2 = new Programmer(2, "alex", 2000);
		System.out.println(p1.toString());
		System.out.println(p2.toString());
	}

}
