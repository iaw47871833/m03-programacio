package InheritancePackage;

public class Design extends Employee {
	// Attributes
	// Weekly wages
	private double weekWage;
	// Weeks worked
	private int weeksWorked;
	
	/**
	 * @param id
	 * @param name
	 * @param age
	 * @param weekWage
	 * @param weeksWorked
	 */
	public Design(int id, String name, int age, double weekWage, int weeksWorked) {
		super(id, name, age);
		this.weekWage = weekWage;
		this.weeksWorked = weeksWorked;
	}
	
	/**
	 * getSalary
	 * 
	 * Get employee salary
	 * 
	 * @param args Not used
	 * @return salary
	 */ 
	public double getSalary() {
		return this.weekWage * this.weeksWorked;
	}
	

}
