package InheritancePackage;

public class Tech extends Employee {
	
	// Attributes
	// Hour wages
	private double hourWage;
	// Hours worked
	private int hoursWorked;
	
	
	
	public Tech(int id, String name, int age, double hourWage, int hoursWorked) {
		super(id, name, age);
		this.hourWage = hourWage;
		this.hoursWorked = hoursWorked;
	}
	
	/**
	 * getSalary
	 * 
	 * Get employee salary
	 * 
	 * @param args Not used
	 * @return salary
	 */ 
	public double getSalary() {
		return this.hourWage * this.hoursWorked;
	}
	 
}
