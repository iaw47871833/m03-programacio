package InheritancePackage;

public class HR extends Employee {
	// Attributes
	// Monthly wages
	private double monthWage;
	// Months worked
	private int monthsWorked;
	
	/**
	 * @param id
	 * @param name
	 * @param age
	 * @param monthWage
	 * @param monthsWorked
	 */
	public HR(int id, String name, int age, double monthWage, int monthsWorked) {
		super(id, name, age);
		this.monthWage = monthWage;
		this.monthsWorked = monthsWorked;
	}

	/**
	 * getSalary
	 * 
	 * Get employee salary
	 * 
	 * @param args Not used
	 * @return salary
	 */ 
	public double getSalary() {
		return this.monthWage * this.monthsWorked;
	}
}
