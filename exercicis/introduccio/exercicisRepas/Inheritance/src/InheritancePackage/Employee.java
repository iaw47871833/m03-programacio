package InheritancePackage;

public class Employee {
	// Attributes
	// Employee ID
	private int id;
	// Employee name
	private String name;
	// Employee age;
	private int age;
	
	/**
	 * @param id
	 * @param name
	 * @param age
	 */
	public Employee(int id, String name, int age) {
		this.id = id;
		this.name = name;
		this.age = age;
	}

	/**
	 * setData
	 * 
	 * @param id employee's id
	 * @param name employee's name
	 * @param age employee's age
	 */ 
	 public void setData(int id, String name, int age) {
		 this.id = id;
		 this.name = name;
		 this.age = age;
	 }
	 
	 /**
	 * getData
	 * 
	 * @param args Not used
	 */ 
	 public String getData() {
		 return "Employee " + this.id + ": Name: " + this.name + ", Age: " + this.age;
	 } 
}
