package InheritancePackage;

public class Main {

	public static void main(String[] args) {
		// Instance of objects
		Tech t1 = new Tech(1, "Ryan", 34, 15, 8);
		Design d1 = new Design(2, "Sara", 29, 40, 4);
		HR h1 = new HR(3, "Edward", 40, 1000, 12);
		
		System.out.println("---- Employee ----");
		System.out.println("------------------");
		
		// Get data
		System.out.println(t1.getData());
		System.out.println(d1.getData());
		System.out.println(h1.getData());
		
		System.out.println("---------------------");
		
		// Set data
		t1.setData(5, "Ryan", 24);
		d1.setData(3, "Rose", 30);
		h1.setData(4, "Paul", 32);
		
		// Get data
		System.out.println(t1.getData());
		System.out.println(d1.getData());
		System.out.println(h1.getData());
		
		System.out.println("---------------------");
		
		// Get salary
		System.out.println("Salary of " + t1.getData() + " => " + t1.getSalary() + " per ");
		System.out.println("Salary of " + d1.getData() + " => " + d1.getSalary());
		System.out.println("Salary of " + h1.getData() + " => " + h1.getSalary());
	}

}
