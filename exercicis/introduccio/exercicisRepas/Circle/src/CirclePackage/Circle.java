package CirclePackage;

public class Circle {
	// Attributes
	// Circle's radius
	private double radius;

	/**
	 * @param radius
	 */
	public Circle(double radius) {
		this.radius = radius;
	}
	
	/**
	 * area
	 * 
	 * Calculate the area of a circle
	 * 
	 * @param args Not used
	 * @return area of the circle
	 */
	public double area() {
		return Math.round((Math.PI * this.radius * this.radius) * 100) / 100;
	}
	
	/**
	 * perimeter
	 * 
	 * Calculate the perimeter of a circle
	 * 
	 * @param args Not used
	 * @return perimeter of the circle
	 */
	public double perimeter() {
		return Math.round((2 * Math.PI * this.radius) * 100) / 100;
	}
	
	@Override
	public String toString() {
		return "Circle [radius=" + radius + ", area()=" + area() + ", perimeter()=" + perimeter() + "]";
	}

	/**
	 * @return the radius
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	
}
