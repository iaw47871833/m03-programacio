package Exercici_reforç_de_1r;

public class Departament {
	// Atributs
	private int id;
	private String nom;
	private String descripcio;
	/**
	 * @param id
	 * @param nom
	 * @param descripcio
	 */
	public Departament(int id, String nom, String descripcio) {
		this.id = id;
		this.nom = nom;
		this.descripcio = descripcio;
	}
}
