package Exercici_reforç_de_1r;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class IKSRotarran {

	public static void main(String[] args) {
		// Formatador de dates
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		// Data d'alta del capità en String
		String dataAltaCapita = "15-08-1954 00:01";
		// Transformem la data del capita a LocalDateTime
		LocalDateTime dataAltaCapitaLDT = LocalDateTime.parse(dataAltaCapita, formatter);
		// Creació del capità
		Oficial capita = new Oficial("001-A", "Martok", true, dataAltaCapitaLDT, 1, 1, true, "Capitanejar la nau");
		// Creació d'un mariner
		// Data d'alta del capità en String
		String dataAltaMariner = "26-12-1981 13:42";
		// Transformem la data del capita a LocalDateTime
		LocalDateTime dataAltaMarinerLDT = LocalDateTime.parse(dataAltaMariner, formatter);
		Mariner mariner_02_03 = new Mariner("758-J", "Kurak", true, dataAltaMarinerLDT, 3, 1, true,
				"Mariner encarregat del timó i la navegació durant el 2n torn");

		// Treure per pantalla el departament del capita
		System.out.println("Departament del capita: " + capita.departament);
		// Treure per pantalla la descripcio de la feina que fa el capita
		// No es pot accedir es un atribut privat que nomes la propia clase pot veure
		// System.out.println("Descripció feina capita: " + capita.descripcioFeina);
		// Treure per pantalla la descripcio de feina del capita
		System.out.println("Descripció feina capita: " + capita.getDescripcioFeina());
		// Aquesta vegada podem accedir perquè utilitzem un métode public com pot ser un
		// getter
		// Imprimim les dades del capità
		capita.imprimirDadesTripulant();
		// Canviar el departament del capità al nº10
		capita.departament = 10;
		capita.imprimirDadesTripulant();

		// Creacio d'un oficial de tipus tripulant
		// Data d'alta del tripulant en String
		String dataAltaTripulant = "22-03-1954 00:01";
		// Transformem la data del tripulant a LocalDateTime
		LocalDateTime dataAltaTripulantLDT = LocalDateTime.parse(dataAltaTripulant, formatter);
		Tripulant oficialDeTipusTripulant = new Tripulant("431-D", "Rob", true, dataAltaTripulantLDT, 3, 1);

		// Creacio d'un oficial tipus oficial
		// Creacio d'un oficial de tipus tripulant
		// Data d'alta del tripulant en String
		String dataAltaOficial = "19-10-1954 00:01";
		// Transformem la data del tripulant a LocalDateTime
		LocalDateTime dataAltaOficialLDT = LocalDateTime.parse(dataAltaOficial, formatter);
		Oficial oficialDeTipusOficial = new Oficial("753-V", "Max", true, dataAltaOficialLDT, 2, 1, true, "Oficial de tipus oficial");
		// Cridem els metodes saludar
		oficialDeTipusTripulant.saludar(); // Crida al saludar de tipus tripulant
		oficialDeTipusOficial.saludar(); // Crida al saludar de tipus oficial
		//System.out.println(IKSRotarranConstants.LLOCS_DE_SERVEI[capita.llocDeServei]);
		// Al haber sobrescribit el toString mostra informació de la clase Oficial
		System.out.println("L'objecte capita (té implementat el toString()): " + capita);
		// Com no s'ha sobreescribit el toString mostra informació de la clase pare (Tripulant)
		System.out.println("L'objecte mariner_02_03 (NO té implementat el toString()): " + mariner_02_03);
		mariner_02_03.imprimirDadesTripulant();
	}

}
