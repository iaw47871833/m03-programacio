package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

public class Cuiner extends Tripulant {

	// Atributs
	private boolean serveiEnElPont;
	private String descripcioFeina;

	/**
	 * @param id
	 * @param nom
	 * @param actiu
	 * @param dataAlta
	 * @param departament
	 * @param llocDeServei
	 * @param serveiEnElPont
	 * @param descripcioFeina
	 */
	public Cuiner(String id, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei,
			boolean serveiEnElPont, String descripcioFeina) {
		super(id, nom, actiu, dataAlta, departament, llocDeServei);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcioFeina = descripcioFeina;
	}

	/**
	 * serveixEnElPont
	 * 
	 * Mostrar si el tripulant serveix en el pont
	 * 
	 * @param args Not used
	 */
	public boolean serveixEnElPont() {
		return this.serveiEnElPont;
	}

}
