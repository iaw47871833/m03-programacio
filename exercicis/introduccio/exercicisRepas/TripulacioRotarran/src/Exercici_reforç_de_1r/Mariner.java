package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

public class Mariner extends Tripulant {

	// Atributs
	private boolean serveiEnElPont;
	private String descripcioFeina;

	/**
		 * @param id
		 * @param nom
		 * @param actiu
		 * @param dataAlta
		 * @param departament
		 * @param llocDeServei
		 * @param serveiEnElPont
		 * @param descripcioFeina
		 */
		public Mariner (String id, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei,
				boolean serveiEnElPont, String descripcioFeina) {
			super(id, nom, actiu, dataAlta, departament, llocDeServei);
			this.serveiEnElPont = serveiEnElPont;
			this.descripcioFeina = descripcioFeina;
		}

	/**
	 * imprimirDadesTripulant
	 * 
	 * Imprimir les dades de la clase tripulant
	 * @param args Not used
	 */
	@Override
	protected void imprimirDadesTripulant() {
		System.out.println("DADES TRIPULANT: ");
		System.out.println("--------------------");
		System.out.println("Bandol: " + BANDOL);
		System.out.println("Id: " + this.id);
		System.out.println("Nom: " + this.nom);
		System.out.println("Actiu: " + this.actiu);
		System.out.println("Departament (de la classe Tripulant): " + this.departament);
		System.out.println("Departament (de la classe IKSRotarranConstants): " + IKSRotarranConstants.DEPARTAMENT[this.departament]);
		System.out.println("Lloc de servei (de la classe Tripulant): " + this.llocDeServei);
		System.out.println("Lloc de servei (de la classe IKSRotarranConstants): " + IKSRotarranConstants.LLOCS_DE_SERVEI[this.llocDeServei]);
		System.out.println("Descripció de la feina que fa: " + this.descripcioFeina);
		System.out.println("Serveix en el pont?: " + this.serveixEnElPont());
		System.out.println("Data alta: " + this.dataAlta);
	}
		
	/**
	 * serveixEnElPont
	 * 
	 * Mostrar si el tripulant serveix en el pont
	 * 
	 * @param args Not used
	 */
	public String serveixEnElPont() {
		return this.serveiEnElPont == true ? "SI" : "NO";
	}

}
