package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

public class Tripulant implements IKSRotarranConstants {
	// Atributs
	static final String BANDOL = "Imperi Klingon";
	protected String id;
	protected String nom;
	protected boolean actiu;
	protected LocalDateTime dataAlta;
	protected int departament;
	protected int llocDeServei;

	/**
	 * @param bandol
	 * @param id
	 * @param nom
	 * @param actiu
	 * @param dataAlta
	 * @param departament
	 * @param llocDeServei
	 */
	public Tripulant(String id, String nom, boolean actiu, LocalDateTime dataAlta, int departament,
			int llocDeServei) {
		this.id = id;
		this.nom = nom;
		this.actiu = actiu;
		this.dataAlta = dataAlta;
		this.departament = departament;
		this.llocDeServei = llocDeServei;
	}
	
	/**
	 * imprimirDadesTripulant
	 * 
	 * Imprimir les dades de la clase tripulant
	 * @param args Not used
	 */
	protected void imprimirDadesTripulant() {
		System.out.println("DADES TRIPULANT: ");
		System.out.println("--------------------");
		System.out.println("Bandol: " + BANDOL);
		System.out.println("Id: " + this.id);
		System.out.println("Nom: " + this.nom);
		System.out.println("Actiu: " + this.actiu);
		System.out.println("Data alta: " + this.dataAlta);
		System.out.println("departament: " + this.departament);
		System.out.println("Lloc de servei: " + this.llocDeServei);
	}
	
	/**
	 * saludar
	 * 
	 * Imprimir per pantalla la salutació del tripulant 
	 * @param args Not used
	 */
	public void saludar() {
		System.out.println("Hola des de la superclasse Tripulant");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (actiu ? 1231 : 1237);
		result = prime * result + ((dataAlta == null) ? 0 : dataAlta.hashCode());
		result = prime * result + departament;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + llocDeServei;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tripulant other = (Tripulant) obj;
		if (actiu != other.actiu)
			return false;
		if (dataAlta == null) {
			if (other.dataAlta != null)
				return false;
		} else if (!dataAlta.equals(other.dataAlta))
			return false;
		if (departament != other.departament)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (llocDeServei != other.llocDeServei)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tripulant [id=" + id + ", nom=" + nom + ", actiu=" + actiu + ", dataAlta=" + dataAlta + ", departament="
				+ departament + ", llocDeServei=" + llocDeServei + "]";
	}
	
}
