package Exercici_reforç_de_1r;

public class LlocDeServei {
	// Atributs
	private int id;
	private String nom;
	private String descripcio;
	
	/**
	 * @param id
	 * @param nom
	 * @param descripcio
	 */
	public LlocDeServei(int id, String nom, String descripcio) {
		this.id = id;
		this.nom = nom;
		this.descripcio = descripcio;
	}

}
