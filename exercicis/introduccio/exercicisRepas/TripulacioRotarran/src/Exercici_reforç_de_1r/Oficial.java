package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

public class Oficial extends Tripulant {
	// Atributs
	private boolean serveiEnElPont;
	private String descripcioFeina;

	/**
	 * @param id
	 * @param nom
	 * @param actiu
	 * @param dataAlta
	 * @param departament
	 * @param llocDeServei
	 * @param serveiEnElPont
	 * @param descripcioFeina
	 */
	public Oficial(String id, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei,
			boolean serveiEnElPont, String descripcioFeina) {
		super(id, nom, actiu, dataAlta, departament, llocDeServei);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcioFeina = descripcioFeina;
	}
	
	/**
	 * serveixEnElPont
	 * 
	 * Mostrar si el tripulant serveix en el pont
	 * @param args Not used
	 */
	public boolean serveixEnElPont() {
		return this.serveiEnElPont;
	}
	
	/**
	 * saludar
	 * 
	 * Imprimir per pantalla la salutació del tripulant 
	 * @param args Not used
	 */
	@Override
	public void saludar() {
		System.out.println("Hola des de la subclasse Oficial");
	}

	/**
	 * @return the descripcioFeina
	 */
	public String getDescripcioFeina() {
		return descripcioFeina;
	}

	/**
	 * @param descripcioFeina the descripcioFeina to set
	 */
	public void setDescripcioFeina(String descripcioFeina) {
		this.descripcioFeina = descripcioFeina;
	}

	@Override
	public String toString() {
		return "Oficial [serveiEnElPont=" + serveiEnElPont + ", descripcioFeina=" + descripcioFeina + ", id=" + id
				+ ", nom=" + nom + ", actiu=" + actiu + ", dataAlta=" + dataAlta + ", departament=" + departament
				+ ", llocDeServei=" + llocDeServei + "]";
	}

	
}
