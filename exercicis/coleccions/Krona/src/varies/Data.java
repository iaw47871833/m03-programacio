package varies;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import Llibreries.Cadena;

public abstract class Data {
	// Formatador de dates
	public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

	public static String imprimirData(LocalDateTime dataTmp) {
		// Formatejador de dates
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		String dataString;
		// Comprobem que la data donada no es null
		// Si no es null, la retornem
		if (dataTmp != null) {
			dataString = dataTmp.format(formatter);
		} else {
			return null;
		}
		return dataString;
	}
	
	/**
	 * esData
	 * 
	 * Donada una data en format DD-MM-YYYY, comprobar si es o no una data
	 * 
	 * @param dataTmp, data temporal
	 * @return true si dataTmp és vàlida, false en cas contrari
	 */
	public static boolean esData(String dataTmp) {
		// Indiquem que la data ha de estar formada per 3 numeros separats
		int numSeparats = 3;
		int contNumeros = 0;
		// Separem la data per guions
		String[] dataSplit = dataTmp.split("-");
		// Recorrem la dataSplit i comprobem que son numeros
		for (int i = 0; i < dataSplit.length; i++) {
			// Si es un número incrementem el contador
			if (Cadena.stringIsInt(dataSplit[i])) {
				contNumeros++;
			}
		}
		// Si esta format per 3 numeros separats per guions, mirem que el dia i mes estan
		// formats per 2 digits i el any per 4
		if (dataSplit.length == numSeparats && contNumeros == numSeparats) {
			if (dataSplit[0].length() == 2 && dataSplit[1].length() == 2 && dataSplit[2].length() == 4) {
				return true;
			}
		}
		return false;
	}
}
