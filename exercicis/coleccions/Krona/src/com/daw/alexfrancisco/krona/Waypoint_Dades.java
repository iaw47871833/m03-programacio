package com.daw.alexfrancisco.krona;

import java.text.Collator;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Locale;

public class Waypoint_Dades implements Comparable<Waypoint_Dades> {
	// Atributs
	private int id; // Clau primaria. Es crea automàticament pel sistema i és intocable.
	private String nom;
	private int[] coordenades;
	private boolean actiu; // TRUE si està actiu i es pot fer servir per afegir-lo a alguna ruta
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio; // Quan actiu passi a valdre FALSE
	private LocalDateTime dataModificacio;
	private int tipus;
	/**
	 * @param id
	 * @param nom
	 * @param coordenades
	 * @param actiu
	 * @param dataCreacio
	 * @param dataAnulacio
	 * @param dataModificacio
	 */
	public Waypoint_Dades(int id, String nom, int[] coordenades, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		this.id = id;
		this.nom = nom;
		this.coordenades = coordenades;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}
	
	public int getTipus() {
		return tipus;
	}

	public void setTipus(int tipus) {
		this.tipus = tipus;
	}

	/**
	 * @param id
	 * @param nom
	 * @param coordenades
	 * @param actiu
	 * @param dataCreacio
	 * @param dataAnulacio
	 * @param dataModificacio
	 * @param tipus
	 */
	public Waypoint_Dades(int id, String nom, int[] coordenades, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio, int tipus) {
		super();
		this.id = id;
		this.nom = nom;
		this.coordenades = coordenades;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
		this.tipus = tipus;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the coordenades
	 */
	public int[] getCoordenades() {
		return coordenades;
	}
	/**
	 * @param coordenades the coordenades to set
	 */
	public void setCoordenades(int[] coordenades) {
		this.coordenades = coordenades;
	}
	/**
	 * @return the actiu
	 */
	public boolean isActiu() {
		return actiu;
	}
	/**
	 * @param actiu the actiu to set
	 */
	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}
	/**
	 * @return the dataCreacio
	 */
	public LocalDateTime getDataCreacio() {
		return dataCreacio;
	}
	/**
	 * @param dataCreacio the dataCreacio to set
	 */
	public void setDataCreacio(LocalDateTime dataCreacio) {
		this.dataCreacio = dataCreacio;
	}
	/**
	 * @return the dataAnulacio
	 */
	public LocalDateTime getDataAnulacio() {
		return dataAnulacio;
	}
	/**
	 * @param dataAnulacio the dataAnulacio to set
	 */
	public void setDataAnulacio(LocalDateTime dataAnulacio) {
		this.dataAnulacio = dataAnulacio;
	}
	/**
	 * @return the dataModificacio
	 */
	public LocalDateTime getDataModificacio() {
		return dataModificacio;
	}
	/**
	 * @param dataModificacio the dataModificacio to set
	 */
	public void setDataModificacio(LocalDateTime dataModificacio) {
		this.dataModificacio = dataModificacio;
	}
	@Override
	public String toString() {
		/*
		int distancia = 0;
		// Calculem la distancia
		for (int i = 0; i < this.coordenades.length; i++) {
			distancia += Math.pow(coordenades[i], 2);
		}*/
		// Mostrem les dades del waypoint si dataCreacio no es null
		if (this.dataCreacio != null) {
			return "WAYPOINT " + this.id + ":\n\t" +
					"nom = " + this.nom +
					"\ncoordenades(x, y, z) = " + Arrays.toString(coordenades) +
					"\nactiu = " + this.actiu +
					"\ndataCreacio = " + this.dataCreacio +
					"\ndataAnulacio = " + this.dataAnulacio +
					"\ndataModificacio = " + this.dataModificacio;
		}
		return "";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Waypoint_Dades other = (Waypoint_Dades) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public int compareTo(Waypoint_Dades w) {
		boolean coorIguals = Arrays.equals(this.getCoordenades(), w.getCoordenades());
		int result;
		// Si son iguals comparem per el seu nom
		// Si no son iguals, determinem la més petita
		if (coorIguals) {
			// Ordenem sense tenir en compte majuscule, minuscules, accents o sense accents
			Collator primaryCollator = Collator.getInstance(new Locale(	"ca"));
			primaryCollator.setStrength(Collator.TERTIARY);
			result = primaryCollator.compare(this.getNom(), w.getNom());
		} else {
			// Guardem els valors de les coordenades i les sumem
			int sumCoor1 = 0, sumCoor2 = 0;
			for (int i = 0; i < this.getCoordenades().length; i++) {
				sumCoor1 += Math.pow(this.getCoordenades()[i], 2);
				sumCoor2 += Math.pow(w.getCoordenades()[i], 2);
			}
			// Comparem els valors
			if (sumCoor1 < sumCoor2) {
				result = -1;
			} else {
				result = 1;
			}
		}
		return result;
	}


}
