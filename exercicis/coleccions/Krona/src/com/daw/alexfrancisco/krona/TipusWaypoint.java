package com.daw.alexfrancisco.krona;

public class TipusWaypoint {
	// Tipus de waypoints
	public final static String[] TIPUS_WAYPOINT = {"planeta", "lluna", "asteroide", "cometa", "meteor", "planetoide", "punt en l'espai"};
}
