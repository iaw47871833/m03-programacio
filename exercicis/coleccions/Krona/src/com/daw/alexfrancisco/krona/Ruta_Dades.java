package com.daw.alexfrancisco.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;

import varies.Data;

public class Ruta_Dades implements Comparable<Ruta_Dades> {
	private int id;                             	
	private String nom;
	private ArrayList<Integer> waypoints;       	
	private boolean actiu;                      	
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;             
	private LocalDateTime dataModificacio;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the waypoints
	 */
	public ArrayList<Integer> getWaypoints() {
		return waypoints;
	}

	/**
	 * @param waypoints the waypoints to set
	 */
	public void setWaypoints(ArrayList<Integer> waypoints) {
		this.waypoints = waypoints;
	}

	/**
	 * @return the actiu
	 */
	public boolean isActiu() {
		return actiu;
	}

	/**
	 * @param actiu the actiu to set
	 */
	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}

	/**
	 * @return the dataCreacio
	 */
	public LocalDateTime getDataCreacio() {
		return dataCreacio;
	}

	/**
	 * @param dataCreacio the dataCreacio to set
	 */
	public void setDataCreacio(LocalDateTime dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

	/**
	 * @return the dataAnulacio
	 */
	public LocalDateTime getDataAnulacio() {
		return dataAnulacio;
	}

	/**
	 * @param dataAnulacio the dataAnulacio to set
	 */
	public void setDataAnulacio(LocalDateTime dataAnulacio) {
		this.dataAnulacio = dataAnulacio;
	}

	/**
	 * @return the dataModificacio
	 */
	public LocalDateTime getDataModificacio() {
		return dataModificacio;
	}

	/**
	 * @param dataModificacio the dataModificacio to set
	 */
	public void setDataModificacio(LocalDateTime dataModificacio) {
		this.dataModificacio = dataModificacio;
	}

	/**
	 * @param id
	 * @param nom
	 * @param waypoints
	 * @param actiu
	 * @param dataCreacio
	 * @param dataAnulacio
	 * @param dataModificacio
	 */
	public Ruta_Dades(int id, String nom, ArrayList<Integer> waypoints, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		super();
		this.id = id;
		this.nom = nom;
		this.waypoints = waypoints;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}
    
    /**
     * compareTo
     * 
     * comparem a partir de si tenen els mateixos waypoints, sino per el seu id
     */
	@Override
	public int compareTo(Ruta_Dades rd) {
		int result;
		// Comparem els waypoints
		if (this.getWaypoints().equals(rd.getWaypoints())) {
			result = 0;
		}
		// Comparem l'id
		else {
			result = rd.getId() - this.getId();
		}
		return result;
	}

	@Override
	public String toString() {
		return "Dades de la ruta:\n\tid = " + this.getId() + 
				":\n\tnom = " + this.getNom() +
				"\n\tactiu = " + this.isActiu() +
				"\n\tdataCreacio = " + Data.imprimirData(this.getDataCreacio()) +
				"\n\tdataAnulacio = " + Data.imprimirData(this.getDataAnulacio()) +
				"\n\tdataModificacio = " + Data.imprimirData(this.getDataModificacio()) +
				"\n\twaypoints = " + this.getWaypoints().toString();
	}
    
}
