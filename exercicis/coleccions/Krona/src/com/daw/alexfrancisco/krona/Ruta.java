package com.daw.alexfrancisco.krona;

import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import Llibreries.Cadena;
import varies.Data;

public class Ruta {
	public static List<Waypoint_Dades> crearRutaInicial() {
		List<Waypoint_Dades> llistaWaypointLinkedList = null;

		llistaWaypointLinkedList = new LinkedList<Waypoint_Dades>();

		llistaWaypointLinkedList.add(new Waypoint_Dades(0, "Òrbita de la Terra", new int[] { 0, 0, 0 }, true,
				LocalDateTime.parse("21-10-2020 01:10", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(1, "Punt Lagrange Terra-LLuna", new int[] { 1, 1, 1 }, true,
				LocalDateTime.parse("21-10-2020 01:00", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(2, "Òrbita de la LLuna", new int[] { 2, 2, 2 }, true,
				LocalDateTime.parse("21-10-2020 00:50", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(3, "Òrbita de Mart", new int[] { 3, 3, 3 }, true,
				LocalDateTime.parse("21-10-2020 00:40", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(4, "Òrbita de Júpiter", new int[] { 4, 4, 4 }, true,
				LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(5, "Punt Lagrange Júpiter-Europa", new int[] { 5, 5, 5 }, true,
				LocalDateTime.parse("21-10-2020 00:20", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(6, "Òrbita de Europa", new int[] { 6, 6, 6 }, true,
				LocalDateTime.parse("21-10-2020 00:10", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(7, "Òrbita de Venus", new int[] { 7, 7, 7 }, true,
				LocalDateTime.parse("21-10-2020 00:01", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter)));

		return llistaWaypointLinkedList;
	}

	/**
	 * inicialitzarRuta
	 * 
	 * Métode per inicialitzar una ruta
	 * 
	 * @param comprovacioRendimentTmp objecte de tipus comprovacioRendimentTmp
	 * @return objecte comprovacioRendimentTmp
	 */
	public static ComprovacioRendiment inicialitzarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		// Cridem el métode crearRutaInicial
		List<Waypoint_Dades> llistaWaypointLinkedList;
		llistaWaypointLinkedList = crearRutaInicial();
		// Recorrem la llista per ficar els elements a pilaWaypoints
		for (Waypoint_Dades wdTmp : llistaWaypointLinkedList) {
			comprovacioRendimentTmp.pilaWaypoints.push(wdTmp);
		}
		// Creem un nou waypoint
		Waypoint_Dades wd = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] { 4, 4, 4 }, true,
				LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter));
		// Afegim el nou waypoint a la pila waypoints
		comprovacioRendimentTmp.pilaWaypoints.addFirst(wd);
		// Fiquem el nou waypoint a wtmp
		comprovacioRendimentTmp.wtmp = wd;
		// Retornem l'objecte comprovacioRendimentTmp
		return comprovacioRendimentTmp;
	}

	/**
	 * visualitzarRuta
	 * 
	 * Mostrar la ruta
	 * 
	 * @param comprovacioRendimentTmp objecte de tipus comprovacioRendiment
	 * @return objecte comprovacioRendimentTmp
	 */
	public static void visualitzarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		// Mostrem els waypoints de la pila
		for (Waypoint_Dades wd : comprovacioRendimentTmp.pilaWaypoints) {
			System.out.println(wd.toString());
		}
	}

	/**
	 * invertirRuta
	 * 
	 * Invertir l'ordre dels waypoints de la ruta emmagatzemada en pilaWaypoints
	 * 
	 * @param comprovacioRendimentTmp objecte de tipus comprovacioRendiment
	 * @return objecte comprovacioRendimentTmp
	 */
	public static void invertirRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		// Declarem la nova pila
		Deque<Waypoint_Dades> pilaWaypointsInversa = new ArrayDeque<>();
		// Recorrem la pila original i per cada element la fiquem a la pila inversa
		while (!comprovacioRendimentTmp.pilaWaypoints.isEmpty()) {
			// L'afegim l'ultim element
			pilaWaypointsInversa.push(comprovacioRendimentTmp.pilaWaypoints.pop());
		}
		// Mostrem la pila inversa
		for (Waypoint_Dades wdTmp : pilaWaypointsInversa) {
			System.out.println(wdTmp.toString());
		}
	}

	/**
	 * existeixWaypointEnRuta
	 * 
	 * Comprobar que el waypoint comprovacioRendimentTmp.wtmp està en
	 * comprovacioRendimentTmp.pilaWaypoints
	 * 
	 * @param comprovacioRendimentTmp objecte de tipus comprovacioRendiment
	 * @return objecte comprovacioRendimentTmp
	 */
	public static void existeixWaypointEnRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		// Guardem el waypoint tmp
		Waypoint_Dades wd = comprovacioRendimentTmp.wtmp;
		// Comprobem si es troba en la pilaWaypoints
		if (comprovacioRendimentTmp.pilaWaypoints.contains(wd)) {
			System.out.println("SI hem trobat el waypoint " + wd.getNom()
					+ " emmagatzemat en comprovacioRendimentTmp.wtmp, en la llista.");
		} else {
			System.out.println("NO hem trobat el waypoint " + wd.getNom()
					+ " emmagatzemat en comprovacioRendimentTmp.wtmp, en la llista.");
		}
		// Creem un nou waypoint
		Waypoint_Dades wd2 = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] { 4, 4, 4 }, true,
				LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter));
		// Comprovem que el nou waypoint esta en la pila
		if (comprovacioRendimentTmp.pilaWaypoints.contains(wd2)) {
			System.out.println("SI hem trobat el waypoint " + wd2.getNom() + " creat ara mateix, en la llista");
		} else {
			System.out.println("NO hem trobat el waypoint " + wd2.getNom() + " creat ara mateix, en la llista");
		}
	}

	/**
	 * inicialitzaLlistaRutes
	 * 
	 * Crear una llista amb rutes per a fer-la servir en els menús 21, 22 i 23
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 * @return objecte ComprovacioRendiment
	 */
	public static ComprovacioRendiment inicialitzarLlistaRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		// Creem les rutes
		Ruta_Dades ruta_0 = new Ruta_Dades(0, "ruta   0:   Terra   -->   Punt   Lagrange   Júpiter-Europa",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5)), true,
				LocalDateTime.parse("28-10-2020 16:30", Data.formatter), null,
				LocalDateTime.parse("28-10-2020 16:30", Data.formatter));
		Ruta_Dades ruta_1 = new Ruta_Dades(1, "ruta 1: Terra --> Òrbita de Mart (directe)",
				new ArrayList<Integer>(Arrays.asList(0, 3)), true,
				LocalDateTime.parse("28-10-2020 16:31", Data.formatter), null,
				LocalDateTime.parse("28-10-2020 16:31", Data.formatter));
		Ruta_Dades ruta_2 = new Ruta_Dades(2, "ruta 2.1: Terra --> Òrbita de Venus",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true,
				LocalDateTime.parse("28-10-2020 16:32", Data.formatter), null,
				LocalDateTime.parse("28-10-2020 16:32", Data.formatter));
		Ruta_Dades ruta_3 = new Ruta_Dades(3, "ruta 3: Terra --> Mart (directe) --> Òrbita de Júpiter ",
				new ArrayList<Integer>(Arrays.asList(0, 3, 4)), true,
				LocalDateTime.parse("28-10-2020 16:33", Data.formatter), null,
				LocalDateTime.parse("28-10-2020 16:33", Data.formatter));
		Ruta_Dades ruta_4 = new Ruta_Dades(4, "ruta   2.2:   Terra   -->   Òrbita   de   Venus   (REPETIDA)",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true,
				LocalDateTime.parse("28-10-2020 16:32", Data.formatter), null,
				LocalDateTime.parse("30-10-2020 19:49", Data.formatter));
		// Afegim les rutes a la llista de rutes de comprovacioRendimentTmp
		comprovacioRendimentTmp.llistaRutes.add(ruta_0);
		comprovacioRendimentTmp.llistaRutes.add(ruta_1);
		comprovacioRendimentTmp.llistaRutes.add(ruta_2);
		comprovacioRendimentTmp.llistaRutes.add(ruta_3);
		comprovacioRendimentTmp.llistaRutes.add(ruta_4);
		// Recorrem les rutes i les mostrem
		for (Ruta_Dades rd : comprovacioRendimentTmp.llistaRutes) {
			System.out.println(rd.getNom() + ": waypoints" + rd.getWaypoints().toString());
		}
		return comprovacioRendimentTmp;
	}

	/**
	 * setUnio
	 * 
	 * Ficar tots els waypoints que hi ha en totes les rutes de
	 * comprovacioRendimentTmp.llistaRutes dins d'un HashSet
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 */
	public static void setUnio(ComprovacioRendiment comprovacioRendimentTmp) {
		// Inicialitzem el HashSet
		Set<Integer> llistaWaypoints = new HashSet<Integer>();
		// Fiquem els waypoints que hi ha en les rutes en el hashset
		for (Ruta_Dades rd : comprovacioRendimentTmp.llistaRutes) {
			// Afegim les rutes
			llistaWaypoints.addAll(rd.getWaypoints());
		}
		// Els mostrem
		System.out.println("ID dels waypoints ficats en el set: " + llistaWaypoints.toString());
	}

	/**
	 * setInterseccio
	 * 
	 * Ficar només els waypoints que estan en totes le rutes de
	 * comprovacioRendimentTmp.llistaRutes dins d'un HashSet
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 */
	public static void setInterseccio(ComprovacioRendiment comprovacioRendimentTmp) {
		// Inicialitzem el HashSet
		Set<Integer> totsElements = new HashSet<Integer>();
		Set<Integer> intersecWaypoints = new HashSet<Integer>();
		// Fiquem els waypoints que estan en totes les rutes
		for (Ruta_Dades rd : comprovacioRendimentTmp.llistaRutes) {
			// Afegim les rutes
			totsElements.addAll(rd.getWaypoints());
		}
		for (Ruta_Dades rd : comprovacioRendimentTmp.llistaRutes) {
			// Afegim les rutes
			totsElements.retainAll(rd.getWaypoints());
		}
		// Els mostrem
		System.out.println("ID dels waypoints en totes les rutes: " + totsElements.toString());
	}

	/**
	 * buscarRuta
	 * 
	 * @param numRuta                 id de la ruta a buscar
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 * @return posició que ocupa la ruta que busquem dins de l'arraylist
	 *         comprovacioRendimentTmp.llistaRutes
	 */
	private static int buscarRuta(int numRuta, ComprovacioRendiment comprovacioRendimentTmp) {
		// Inicialitzem la posició
		int pos = -1;
		int contador = 0;
		// Recorrem les rutes
		for (Ruta_Dades rd : comprovacioRendimentTmp.llistaRutes) {
			contador++;
			// Agafem el id de la ruta i la comparem amb la que es vol buscar
			if (rd.getId() == numRuta) {
				pos = contador;
				// Tallem l'execució si el trobem
				break;
			}
		}
		return pos;
	}

	/**
	 * setResta
	 * 
	 * seleccionar 2 rutes de les que hi ha en comprovacioRendimentTmp.llistaRutes i
	 * de ficar dins d'un HashSet els waypoints que estan en la ruta A i no en la B
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 */
	public static void setResta(ComprovacioRendiment comprovacioRendimentTmp) {
		// Declarem l'Scanner
		Scanner sc = new Scanner(System.in);
		// Printem les rutes actuals (ID, nom i waypoinys)
		System.out.println("Rutes actuals:");
		for (Ruta_Dades rd : comprovacioRendimentTmp.llistaRutes) {
			System.out.println("Id " + rd.getId() + ": " + rd.getNom() + ": waypoints" + rd.getWaypoints().toString());
		}
		// Demanem 2 rutes a l'usuari separats per espais
		System.out.println("Selecciona ruta A i B (format: 3 17):");
		String rutesInput = sc.nextLine();
		// Comprobem que només pasa 2 arguments
		String rutes[] = rutesInput.split(" ");
		int cont = 0; // Fem un contador per saber quans paràmetres son correctes
		if (rutes.length != 2) {
			System.out.println("ERROR: introduir 2 paràmetres separats per 1 espai en blanc. Has introduit "
					+ rutes.length + " paràmetres.");
		} else {
			// Comprobem que son integers
			for (int i = 0; i < rutes.length; i++) {
				// Si la ruta es enter, incrementem el contador
				// En cas contrari mostrem que no es valida
				if (Cadena.stringIsInt(rutes[i])) {
					cont++;
				} else {
					System.out.println(
							"ERROR: has introduït " + rutes[i] + " com a ruta. Els ID de les rutes són integers.");
				}
			}
		}
		// Si el contador de enters es igual a 2 comprobem que les rutes donades estan
		// incluides
		boolean guardarWaypoints = true;
		if (cont == 2) {
			// Recorrem les rutes
			for (int i = 0; i < rutes.length; i++) {
				// Convertim l'string a enter
				int rutaInt = Integer.parseInt(rutes[i]);
				// Comprobem que es troba incluida
				if (buscarRuta(rutaInt, comprovacioRendimentTmp) == -1) {
					System.out.println("ERROR: no existeix la ruta " + rutaInt + " en el sistema.");
					guardarWaypoints = false;
				}
			}
			if (guardarWaypoints) {
				// Guardem els waypoints de les rutes
				ArrayList<Integer> rutaA = comprovacioRendimentTmp.llistaRutes.get(Integer.parseInt(rutes[0]))
						.getWaypoints();
				ArrayList<Integer> rutaB = comprovacioRendimentTmp.llistaRutes.get(Integer.parseInt(rutes[1]))
						.getWaypoints();
				// Inicialitzem el hashSet
				Set<Integer> resta = new HashSet<Integer>();
				// Afegim els waypoints de de la ruta A
				resta.addAll(rutaA);
				System.out.println("HashSet (havent-hi afegir els waypoints de la ruta A) = " + resta.toString());
				// Treiem els waypoints de la ruta B
				resta.removeAll(rutaB);
				System.out.println("HashSet (havent-hi tret els waypoints de la ruta B) = " + resta.toString());
			}
		}
	}

	/**
	 * crearSetOrdenatDeRutes
	 * 
	 * Ordenar les rutes de comprovacioRendimentTmp.llistaRutes en funció de: Si 2
	 * rutes tenene els mateixos waypoints, a llavors pel sistema seran la mateixa
	 * ruta Si 2 rutes no tenen els mateixos waypoints, a llavors s'ordenaran per
	 * l'ID (de ID més gran a més petit)
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 */
	public static void crearSetOrdenatDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		// Inicialitzem la llista
		SortedSet<Ruta_Dades> rutesOrdenats = new TreeSet<Ruta_Dades>();
		// Afegim les rutes
		rutesOrdenats.addAll(comprovacioRendimentTmp.llistaRutes);
		// Mostres les rutes
		for (Ruta_Dades rd : rutesOrdenats) {
			System.out.println("ID " + rd.getId() + ": " + rd.getNom() + " : waypoints" + rd.getWaypoints());
		}
	}

	/**
	 * crearLinkedHashMapDeRutes
	 * 
	 * Crear un map on ficarem totes les rutes creades en el menú 20. Poden existir
	 * rutes amb les mateixes dades (menys el ID, es clar). Creem el mapa que
	 * contingui l'ordre temporal en que es van inserir les rutes. Finalment les
	 * visualitzem de 3 formes i mostrem quan trigan.
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void crearLinkedHashMapDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		// Inicialitzem les variables
		long tempsInicial;
		long tempsFinal;
		int temps1aForma;
		int temps2aForma;
		int temps3aForma;
		LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes;
		int clauDelMap;

		mapaLinkedDeRutes = new LinkedHashMap<Integer, Ruta_Dades>();
		// Afegim les rutes al mapa linked de rutes
		for (Ruta_Dades rutaTmp : comprovacioRendimentTmp.llistaRutes) {
			mapaLinkedDeRutes.put(rutaTmp.getId(), rutaTmp);
		}

		// 1a forma de visualitzar el contingut del map.
		// Fem un set amb les entrades del map i el recorrem. Es crea un set on el seu
		// contingut són les entrades del mapa.
		tempsInicial = System.nanoTime();
		Set setTmp = mapaLinkedDeRutes.entrySet();
		Iterator it1 = setTmp.iterator();
		System.out.println("1a forma de visualitzar el contingut del map (map --> set + iterador del set):");
		while (it1.hasNext()) {
			Map.Entry me = (Map.Entry) it1.next();
			System.out.println("Clau del map = " + me.getKey() + ": \n" + me.getValue().toString());
		}
		tempsFinal = System.nanoTime();
		temps1aForma = (int) ((tempsFinal - tempsInicial) / 1000);

		// 2a forma de visualitzar el contingut del map.
		// Fem un iterador que navegui per les claus del mapa. Es crea un set on el seu
		// contingut són les claus del map.
		tempsInicial = System.nanoTime();
		System.out.println();
		System.out.println("2a forma de visualitzar el contingut del map (iterator de les claus del map):");
		Iterator<Integer> it2 = mapaLinkedDeRutes.keySet().iterator();
		while (it2.hasNext()) {
			clauDelMap = it2.next();
			System.out.println(clauDelMap + ": " + mapaLinkedDeRutes.get(clauDelMap));
		}
		tempsFinal = System.nanoTime();
		temps2aForma = (int) ((tempsFinal - tempsInicial) / 1000);

		// 3a forma de visualitzar el contingut del map.
		// Lo mateix que la forma 1 però fent servir un bucle for en comptes d'un
		// iterator.
		tempsInicial = System.nanoTime();
		System.out.println();
		System.out.println("3a forma de visualitzar el contingut del map (for-each del map --> set):");
		for (Entry<Integer, Ruta_Dades> dada : mapaLinkedDeRutes.entrySet()) {
			System.out.println(dada.getKey() + ": " + dada.getValue().toString());
		}
		tempsFinal = System.nanoTime();
		temps3aForma = (int) ((tempsFinal - tempsInicial) / 1000);

		System.out.println();
		System.out.println("TEMPS PER 1a FORMA (map --> set + iterador del set): " + temps1aForma);
		System.out.println("TEMPS PER 2a FORMA (iterator de les claus del map): " + temps2aForma);
		System.out.println("TEMPS PER 3a FORMA (for-each del map --> set): " + temps3aForma);

		comprovacioRendimentTmp.mapaLinkedDeRutes.putAll(mapaLinkedDeRutes);
		// No fem un return de comprovacioRendimentTmp perquè en realitat quan el rebem
		// per paràmetre, estem rebent un apuntador i no
		// el propi objecte.
		// EL PROBLEMA ÉS QUE A LLAVORS NO SABEM QUINS MÈTODES MODIFIQUEN EL CONTINGUT
		// DE comprovacioRendimentTmp A MENYS QUE
		// MIREM EL SEU CODI.
	}

	/**
	 * visualitzarRutesDelMapAmbUnWaypointConcret
	 * 
	 * Recorrer les rutes emmagatzemades en
	 * comprovacioRendimentTmp.mapaLinkedDeRutes i les que tinguin el waypoint que
	 * hagi ficat l'usuari, les printem per pantalla. Ens assegurem que l'usuari
	 * fica un número quan li demanem el waypoint
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 * 
	 */
	public static void visualitzarRutesDelMapAmbUnWaypointConcret(ComprovacioRendiment comprovacioRendimentTmp) {
		// Declaració de l'Scanner
		Scanner sc = new Scanner(System.in);
		// Demanem un número a l'usuari
		System.out.print("Escriu el nº del waypoint que vols buscar:");
		String numWaypoint = sc.nextLine();
		boolean numValid = false;
		// Comprobem que es un numero
		if (!Cadena.stringIsInt(numWaypoint)) {
			System.out
					.println("ERROR: has introduit " + numWaypoint + " com a ruta. Els ID de les rutes són integers.");
		} else {
			numValid = true;
		}
		// Continuem demanant a l'usuari en el cas de que no sigui un enter
		while (!numValid) {
			System.out.print("Escriu el nº del waypoint que vols buscar:");
			numWaypoint = sc.nextLine();
			if (!Cadena.stringIsInt(numWaypoint)) {
				System.out.println(
						"ERROR: has introduit " + numWaypoint + " com a ruta. Els ID de les rutes són integers.");
			} else {
				numValid = true;
			}
		}
		// Si el número es valid recorrem comprovacioRendimentTmp.mapaLinkedDeRutes
		if (numValid) {
			// Transformem el número introduit a enter
			int numWaypointInt = Integer.parseInt(numWaypoint);
			for (Entry<Integer, Ruta_Dades> rd : comprovacioRendimentTmp.mapaLinkedDeRutes.entrySet()) {
				// Per cada ruta, comprobem si els seus waypoints contenen el id donat
				// En cas afirmatiu, el mostrem
				if (rd.getValue().getWaypoints().contains(numWaypointInt)) {
					System.out.println(rd.getKey() + ": " + rd.getValue().toString());
				}
			}
		}
	}

	/**
	 * esborrarRutesDelMapAmbUnWaypoint
	 * 
	 * Recorrer les rutes emmagatzemades en
	 * comprovacioRendimentTmp.mapaLinkedDeRutes i les que tinguin el waypoint que
	 * hagi ficat l'usuari, les printem per pantalla i les esborrem. Ens assegurem
	 * que hagi ficat un número
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void esborrarRutesDelMapAmbUnWaypointConcret(ComprovacioRendiment comprovacioRendimentTmp) {
		// Declaració de l'Scanner
		Scanner sc = new Scanner(System.in);
		// Demanem un número a l'usuari
		System.out.print("Escriu el nº del waypoint que vols buscar:");
		String numWaypoint = sc.nextLine();
		boolean numValid = false;
		// Comprobem que es un numero
		if (!Cadena.stringIsInt(numWaypoint)) {
			System.out
					.println("ERROR: has introduit " + numWaypoint + " com a ruta. Els ID de les rutes són integers.");
		} else {
			numValid = true;
		}
		// Continuem demanant a l'usuari en el cas de que no sigui un enter
		while (!numValid) {
			System.out.print("Escriu el nº del waypoint que vols buscar:");
			numWaypoint = sc.nextLine();
			if (!Cadena.stringIsInt(numWaypoint)) {
				System.out.println(
						"ERROR: has introduit " + numWaypoint + " com a ruta. Els ID de les rutes són integers.");
			} else {
				numValid = true;
			}
		}
		// Si el número es valid recorrem comprovacioRendimentTmp.mapaLinkedDeRutes
		if (numValid) {
			// Transformem el número introduit a enter
			int numWaypointInt = Integer.parseInt(numWaypoint);
			System.out.println("RUTES ESBORRADES QUE CONTENEN EL WAYPOINT " + numWaypointInt + ":");
			// Recorrem les rutes
			Iterator<Integer> it = comprovacioRendimentTmp.mapaLinkedDeRutes.keySet().iterator();
			while (it.hasNext()) {
				int clauDelMap = it.next();
				Ruta_Dades rutaTmp = comprovacioRendimentTmp.mapaLinkedDeRutes.get(clauDelMap);
				// Comprobem que conte el waypoint
				if (rutaTmp.getWaypoints().contains(numWaypointInt)) {
					// El mostrem i l'eliminem
					System.out.println(rutaTmp.toString());
					it.remove();
				}
			}
		}
	}

	/**
	 * visualitzarUnaRutaDelMap
	 * 
	 * Demanem el ID d'una ruta, llavors la busquem en
	 * comprovacioRendimentTmp.mapaLinkedDeRutes i la per pantalla. Ens assegurem
	 * que l'usuari fica un número.
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 */
	public static void visualitzarUnaRutaDelMap(ComprovacioRendiment comprovacioRendimentTmp) {
		// Inicialitzem l'Scanner
		Scanner sc = new Scanner(System.in);
		// Demanem l'id a l'usuari
		System.out.print("Escriu el nº del waypoint que vols buscar: ");
		String idRuta = sc.next();
		boolean numValid = false;
		// Comprobem que es un numero
		if (!Cadena.stringIsInt(idRuta)) {
			System.out.println("ERROR: has introduit " + idRuta + " com a ruta. Els ID de les rutes són integers.");
		} else {
			numValid = true;
		}
		// Continuem demanant a l'usuari en el cas de que no sigui un enter
		while (!numValid) {
			System.out.print("Escriu el nº del waypoint que vols buscar:");
			idRuta = sc.nextLine();
			if (!Cadena.stringIsInt(idRuta)) {
				System.out.println("ERROR: has introduit " + idRuta + " com a ruta. Els ID de les rutes són integers.");
			} else {
				numValid = true;
			}
		}
		// Si el número és valid, mostrem la ruta amb aquest id
		if (numValid) {
			// Transformem el id a integer
			int idRutaInt = Integer.parseInt(idRuta);
			for (Entry<Integer, Ruta_Dades> rd : comprovacioRendimentTmp.mapaLinkedDeRutes.entrySet()) {
				// Per cada ruta, comprobem si els seus waypoints contenen el id donat
				// En cas afirmatiu, el mostrem
				if (rd.getValue().getId() == idRutaInt) {
					System.out.println(rd.getValue().toString());
				}
			}
		}
	}

	/**
	 * ordenarRutesMapPerID
	 * 
	 * Utilitzar un TreeMap on les claus siguin els ID de les rutes i els valors les
	 * pròpies rutes
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 */
	public static void ordenarRutesMapPerID(ComprovacioRendiment comprovacioRendimentTmp) {
		// Inicialitzem la llista
		SortedMap<Integer, Ruta_Dades> rutesOrdenats = new TreeMap<Integer, Ruta_Dades>();
		// Afegim les rutes
		rutesOrdenats.putAll(comprovacioRendimentTmp.mapaLinkedDeRutes);
		// Les mostrem
		for (Entry<Integer, Ruta_Dades> rd : comprovacioRendimentTmp.mapaLinkedDeRutes.entrySet()) {
			System.out.println(rd.getValue().toString());
		}
	}

	/**
	 * ordenarRutesMapPerWaypointsAndID
	 * 
	 * Fer servir un TreeMap on les claus siguin els ID de les rutes i els valors
	 * les pròpies rutes. Ordenarem les rutes en funció de si tenen els mateixos
	 * waypoints i si es aixi, a llavors per ID
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 */
	public static void ordenarRutesMapPerWaypointsAndID(ComprovacioRendiment comprovacioRendimentTmp) {
		// Inicialitzem la llista
		SortedMap<Integer, Ruta_Dades> rutes;
		rutes = new TreeMap<Integer, Ruta_Dades>();
		// Afegim les rutes
		rutes.putAll(comprovacioRendimentTmp.mapaLinkedDeRutes);
		// Les mostrem
		for (Entry<Integer, Ruta_Dades> rd : comprovacioRendimentTmp.mapaLinkedDeRutes.entrySet()) {
			System.out.println(rd.getValue().toString());
		}
	}
}
