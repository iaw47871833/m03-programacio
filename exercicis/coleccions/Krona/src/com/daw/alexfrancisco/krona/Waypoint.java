package com.daw.alexfrancisco.krona;

import java.text.Collator;
//import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import Llibreries.Cadena;
import varies.Data;

public class Waypoint {
	/**
	 * inicialitzarComprovacioRendiment
	 * 
	 * Crear un objecte de tipus ComprovacioRendiment
	 * 
	 * @param args Not used
	 * @return objecte ComprovacioRendiment
	 */
	public static ComprovacioRendiment inicialitzarComprovacioRendiment() {
		ComprovacioRendiment comprovacioRendimentTmp = new ComprovacioRendiment();
		return comprovacioRendimentTmp;
	}

	/**
	 * comprovacioRendimentInicialització
	 * 
	 * @param numObjACrear            número de waypoints a ficar
	 * @param comprovacioRendimentTmp objecte a inicialitzar
	 * @return objecte ComprovacioRendiment
	 */
	public static ComprovacioRendiment comprovarRendimentInicialitzacio(int numObjACrear,
			ComprovacioRendiment comprovacioRendimentTmp) {
		// Formatador de dates
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		// Data de creacio en String
		String dataCreacioStr = "15-08-1954 00:01";
		// Transformem la data a LocalDateTime
		LocalDateTime dataCreacioLDT = LocalDateTime.parse(dataCreacioStr, formatter);
		// Temps abans d'inserció de waypoint
		long tempsAbansArrayList = System.nanoTime();
		// Bucle per afegir l'objecte Waypoint_Dades a les llistes de l'objecte
		// ComprovacioRendiment
		for (int i = 0; i < numObjACrear; i++) {
			comprovacioRendimentTmp.llistaArrayList.add(new Waypoint_Dades(0, "Òrbita de la Terra",
					new int[] { 0, 0, 0 }, true, dataCreacioLDT, null, null));
		}
		long tempsDespresArrayList = System.nanoTime();
		// Fem la resta i la passem a milisegons
		long diferenciaArrayList = tempsDespresArrayList - tempsAbansArrayList;
		long duracioArrayList = TimeUnit.MILLISECONDS.convert(diferenciaArrayList, TimeUnit.NANOSECONDS);
		System.out.println("Temps per a insertar " + numObjACrear + " waypoints en l'ArrayList: " + duracioArrayList);

		long tempsAbansLinkedList = System.nanoTime();
		for (int i = 0; i < numObjACrear; i++) {
			comprovacioRendimentTmp.llistaLinkedList.add(new Waypoint_Dades(0, "Òrbita de la Terra",
					new int[] { 0, 0, 0 }, true, dataCreacioLDT, null, null));
		}
		// Temps després d'inserció de waypoint
		long tempsDespresLinkedList = System.nanoTime();
		// Fem la resta i la passem a milisegons
		long diferenciaLinkedList = tempsDespresLinkedList - tempsAbansLinkedList;
		long duracioLinkedList = TimeUnit.MILLISECONDS.convert(diferenciaLinkedList, TimeUnit.NANOSECONDS);
		// Mostrem les duracions
		System.out
				.println("Temps per a insertar " + numObjACrear + " waypoints en el LinkedList: " + duracioLinkedList);
		// Retornem l'objecte comprovacioRendimentTmp
		return comprovacioRendimentTmp;
	}

	/**
	 * comprovacioRendimentInsercio
	 * 
	 * Insertar un nou waypoint en la 1a posició, al mig i al final de les llistes.
	 * A més comprobar quant de temps triga en cada cas (nanosegons)
	 * 
	 * @param comprovacioRendimentTmp
	 * @return objecte comprovacioRendiment
	 */

	public static ComprovacioRendiment comprovarRendimentInsercio(ComprovacioRendiment comprovacioRendimentTmp) {
		// Formatador de dates
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"); // Data de creacio en String
		String dataCreacioStr = "15-08-1954 00:01"; // Transformem la data a LocalDateTime
		LocalDateTime dataCreacioLDT = LocalDateTime.parse(dataCreacioStr, formatter);
		Waypoint_Dades wd = new Waypoint_Dades(0, "Òrbita de la Terra", new int[] { 0, 0, 0 }, true, dataCreacioLDT,
				null, null);
		// Guardem la meitat del array
		int meitat = comprovacioRendimentTmp.llistaArrayList.size() / 2;
		// Guardem l'ultima posició de l'array
		int ultimaPos = comprovacioRendimentTmp.llistaArrayList.size() - 1;
		System.out.println("llistarArrayList.size(): " + comprovacioRendimentTmp.llistaArrayList.size()
				+ ", meitatLlista: " + meitat);
		// Guardem els temps abans d'insertar a array list
		long primerPosArrayListAbans = System.nanoTime();
		// Insertem a la primera posicio de l'arrayList
		comprovacioRendimentTmp.llistaArrayList.add(0, wd);
		// Guardem els temps despres d'insertar a array list
		long primerPosArrayListDespres = System.nanoTime();
		// Calculem la diferencia
		long diferenciaPrimerPosArrayList = primerPosArrayListDespres - primerPosArrayListAbans;
		// Mostrem la diferencia de la primera posició
		System.out.println(
				"Temps per a insertar 1 waypoint en la 1a posició de l'ArrayList: " + diferenciaPrimerPosArrayList);

		// Guardem els temps abans d'insertar a array list
		long primerPosLinkedListAbans = System.nanoTime();
		// Insertem a la primera posicio de l'arrayList
		comprovacioRendimentTmp.llistaLinkedList.add(0, wd);
		// Guardem els temps despres d'insertar al linked list
		long primerPosLinkedListDespres = System.nanoTime();
		// Calculem la diferencia
		long diferenciaPrimerPosLinkedList = primerPosLinkedListDespres - primerPosLinkedListAbans;
		// Mostrem la diferencia de la primera posició
		System.out.println(
				"Temps per a insertar 1 waypoint en la 1a posició del LinkedList: " + diferenciaPrimerPosLinkedList);

		System.out.println("=============");
		// Guardem els temps abans d'insertar a array list
		long migArrayListAbans = System.nanoTime();
		// Insertem a la primera posicio de l'arrayList
		comprovacioRendimentTmp.llistaArrayList.add(meitat, wd);
		// Guardem els temps despres d'insertar a array list
		long migArrayListDespres = System.nanoTime();
		// Calculem la diferencia
		long diferenciaMigArrayList = migArrayListDespres - migArrayListAbans;
		// Mostrem la diferencia de la primera posició
		System.out.println("Temps per a insertar 1 waypoint al mig (pos." + meitat + ") de l'ArrayList: "
				+ diferenciaPrimerPosArrayList);
		// Guardem els temps abans d'insertar a array list
		long migLinkedListAbans = System.nanoTime();
		// Insertem a la primera posicio de l'arrayList
		comprovacioRendimentTmp.llistaLinkedList.add(meitat, wd);
		// Guardem els temps despres d'insertar al linked list
		long migLinkedListDespres = System.nanoTime();
		// Calculem la diferencia
		long diferenciaMigLinkedList = migLinkedListDespres - migLinkedListAbans;
		// Mostrem la diferencia de la primera posició
		System.out.println("Temps per a insertar 1 waypoint al mig (pos." + meitat + ") del LinkedList: "
				+ diferenciaMigLinkedList);

		System.out.println("=============");

		// Guardem els temps abans d'insertar a array list
		long ultimaPosArrayListAbans = System.nanoTime();
		// Insertem a la primera posicio de l'arrayList
		comprovacioRendimentTmp.llistaArrayList.add(ultimaPos, wd);
		// Guardem els temps despres d'insertar a array list
		long ultimaPosArrayListDespres = System.nanoTime();
		// Calculem la diferencia
		long diferenciaUltimaPosArrayList = ultimaPosArrayListDespres - ultimaPosArrayListAbans;
		// Mostrem la diferencia de la primera posició
		System.out.println("Temps per a insertar 1 waypoint al final de l'ArrayList: " + diferenciaUltimaPosArrayList);
		// Guardem els temps abans d'insertar a array list
		long ultimaPosLinkedListAbans = System.nanoTime();
		// Insertem a la primera posicio de l'arrayList
		comprovacioRendimentTmp.llistaLinkedList.add(ultimaPos, wd);
		// Guardem els temps despres d'insertar al linked list
		long ultimaPosLinkedListDespres = System.nanoTime();
		// Calculem la diferencia
		long diferenciaUltimaPosLinkedList = ultimaPosLinkedListDespres - ultimaPosLinkedListAbans;
		// Mostrem la diferencia de la primera posició
		System.out.println("Temps per a insertar 1 waypoint al final del LinkedList: " + diferenciaUltimaPosLinkedList);

		return comprovacioRendimentTmp;
	}

	/**
	 * modificarWaypoints
	 * 
	 * Es faran modificacions dels waypoints de l'ArrayList i del LinkedList
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 * @return objecte ComprovacioRendiment
	 */
	public static ComprovacioRendiment modificarWaypoint(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println("------- APARTAT 1 --------");
		List<Integer> idsPerArrayList = new ArrayList<Integer>();
		// Afegim els numeros
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			idsPerArrayList.add(i);
		}
		System.out.println("S'ha inicialitzat la llista idsPerArrayList amb " + idsPerArrayList.size() + " elements");
		System.out.println("El 1r element té el valor: " + idsPerArrayList.get(0));
		System.out.println("L'últim element té el valor: " + idsPerArrayList.get(idsPerArrayList.size() - 1));

		System.out.println("------- APARTAT 2 --------");
		// Modifiquem els ID's dels waypoints per a donar-los els valors de la llista
		// idsPerArrayList
		for (Integer element : idsPerArrayList) {
			// Abans del canvi d'Id
			System.out.println("ABANS DEL CANVI element: comprovacioRendimentTmp.llistaArrayList.get(" + element
					+ ").getId(): " + comprovacioRendimentTmp.llistaArrayList.get(element).getId());
			// Canviem l'id
			comprovacioRendimentTmp.llistaArrayList.get(element).setId(element);
			// Després del canvi d'Id
			System.out.println("DESPRÉS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get(" + element
					+ ").getId(): " + comprovacioRendimentTmp.llistaArrayList.get(element).getId());
			System.out.println();
		}

		System.out.println("------- APARTAT 3.1 (bucle for) --------");
		// Mostrem el ID i el nom dels waypoints que estan en la llista arrayList
		// Bucle for each
		for (Integer element : idsPerArrayList) {
			System.out.println("ID = " + comprovacioRendimentTmp.llistaArrayList.get(element).getId() + ", nom = "
					+ comprovacioRendimentTmp.llistaArrayList.get(element).getNom());
		}
		// Iterator
		System.out.println("------- APARTAT 3.2 (Iterator) --------");
		Iterator<Waypoint_Dades> itArrayList = comprovacioRendimentTmp.llistaArrayList.iterator();
		// Mentre hi hagi elements, els mostrem
		while (itArrayList.hasNext()) {
			Waypoint_Dades wdAux = itArrayList.next();
			System.out.println("ID = " + wdAux.getId() + ", nom = " + wdAux.getNom());
		}

		System.out.println("------- APARTAT 4 --------");
		System.out.println("Preparat per esborrar el contingut de llistaLinkedList que té "
				+ comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");
		// Esborrem els elements
		comprovacioRendimentTmp.llistaLinkedList.clear();
		System.out.println(
				"Esborrada. Ara llistaLinkedList té " + comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");
		// Copiem els elements de l'arrayList
		for (Waypoint_Dades wd : comprovacioRendimentTmp.llistaArrayList) {
			comprovacioRendimentTmp.llistaLinkedList.add(wd);
		}
		System.out.println("Copiats els elements de llistaArrayList en llistaLinkedList que ara té "
				+ comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");

		// Recorrem l'arraylist a partir de idsPerArrayList
		System.out.println("------- APARTAT 5.1 (bucle for)--------");
		for (Integer element : idsPerArrayList) {
			// Cambiem el nom dels waypoint amb id major de 5
			if (element > 5) {
				// Cambiem el nom
				comprovacioRendimentTmp.llistaArrayList.get(element).setNom("Òrbita de Mart");
				System.out.println("Modificat el waypoint amb id = "
						+ comprovacioRendimentTmp.llistaArrayList.get(element).getId());
			}
		}
		// Comprovem que s'han canviat correctament
		System.out.println("------- APARTAT 5.1 (comprovació)--------");
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			System.out.println("El waypoint amb id = " + comprovacioRendimentTmp.llistaArrayList.get(i).getId()
					+ " té el nom = " + comprovacioRendimentTmp.llistaArrayList.get(i).getNom());
		}

		// Recorrem el linkedList amb iterator
		System.out.println("------- APARTAT 5.2 (Iterator)--------");
		Iterator<Waypoint_Dades> itLinkedList = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (itLinkedList.hasNext()) {
			Waypoint_Dades wdAux = itLinkedList.next();
			// Si el id es menor que 5 canviem el seu nom
			if (wdAux.getId() < 5) {
				wdAux.setNom("Punt Lagrange entre la Terra i la Lluna");
				System.out.println("Modificat el waypoint amb id = " + wdAux.getId());
			}
		}

		// Comprobem el resultat
		System.out.println("------- APARTAT 5.2 (comprovació)--------");
		ListIterator<Waypoint_Dades> listItLinkedList = comprovacioRendimentTmp.llistaLinkedList.listIterator();
		// Recorrem l'iterador cap enrerre
		while (listItLinkedList.hasPrevious()) {
			listItLinkedList.previous();
		}
		// El tornem a recorrer cap endavant
		while (listItLinkedList.hasNext()) {
			Waypoint_Dades wdAux = listItLinkedList.next();
			// Si el id es menor que 5 canviem el seu nom
			System.out.println("El waypoint amb id = " + wdAux.getId() + " té el nom = " + wdAux.getNom());
		}

		return comprovacioRendimentTmp;
	}

	/**
	 * esborrarWaypoints
	 * 
	 * Esborrar waypoints de les llistes
	 * 
	 * @param comprovacioRendimentTmp
	 * @return objecte ComprovacioRendiment
	 */
	public static ComprovacioRendiment esborrarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println("------- APARTAT 1 --------");
		// Recorrem l'array list
		/*
		 * for (Waypoint_Dades wd : comprovacioRendimentTmp.llistaArrayList) { //
		 * Eliminem els que tinguin id menor de 6 if (wd.getId() < 6) {
		 * comprovacioRendimentTmp.llistaArrayList.remove(wd); } }
		 */
		System.out.println(
				"ATENCIÓ: No podem eliminar els waypoints perquè, estem recorrent una colecció i al mateix temps modificant-lo");

		// Recorrem la linkedList amb iterator
		System.out.println("------- APARTAT 2 (Iterator) --------");
		// Creem una nova llista per poder iterar on guardarem els waypoints a eliminar
		List<Waypoint_Dades> eliminar = new LinkedList<Waypoint_Dades>();
		Iterator<Waypoint_Dades> itEliminar = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (itEliminar.hasNext()) {
			Waypoint_Dades wpAux = itEliminar.next();
			// Si id es major a 4, l'afegim a l'array eliminar
			if (wpAux.getId() > 4) {
				eliminar.add(wpAux);
				System.out.println("Esborrat el waypoint amb id = " + wpAux.getId());
			}
		}
		// Els eliminem
		comprovacioRendimentTmp.llistaLinkedList.removeAll(eliminar);
		// Comprovem que s'han eliminat correctament
		System.out.println("------- APARTAT 2 (comprovació)--------");
		for (int i = 0; i < comprovacioRendimentTmp.llistaLinkedList.size(); i++) {
			System.out.println("El waypoint amb id = " + comprovacioRendimentTmp.llistaArrayList.get(i).getId()
					+ " té el nom = " + comprovacioRendimentTmp.llistaArrayList.get(i).getNom());
		}

		System.out.println("------- APARTAT 3 (listIterator)--------");
		// Recorrem linkedlist amb listIterator començant per la primera posició
		ListIterator<Waypoint_Dades> liLinkedList = comprovacioRendimentTmp.llistaLinkedList.listIterator(1);
		Waypoint_Dades wpEliminar = null;
		while (liLinkedList.hasNext()) {
			Waypoint_Dades wdAux = liLinkedList.next();
			// Si l'id es igual a 2, l'esborrem
			if (wdAux.getId() == 2) {
				wpEliminar = wdAux;
			}
		}
		// Eliminem el numero 2
		if (comprovacioRendimentTmp.llistaLinkedList.remove(wpEliminar)) {
			System.out.println("Esborrat el waypoint amb id: " + wpEliminar.getId());
		}
		System.out.println("------- APARTAT 3 (comprovacio)--------");
		// Recorrem la llista desde l'ultima posicio fins a la primera posicio
		ListIterator<Waypoint_Dades> liLinkedListEnrrere = comprovacioRendimentTmp.llistaLinkedList
				.listIterator(comprovacioRendimentTmp.llistaLinkedList.size());
		while (liLinkedListEnrrere.hasPrevious()) {
			Waypoint_Dades wdPrev = liLinkedListEnrrere.previous();
			System.out.println("El waypoint amb id " + wdPrev.getId() + " té el nom = " + wdPrev.getNom());
		}
		return comprovacioRendimentTmp;
	}

	/**
	 * modificarCoordenadesINomDeWaypoints
	 * 
	 * Métode per modificar les coordenades i el nom dels waypoint
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 * @return objecte ComprovacioRendiment
	 * 
	 */
	public static ComprovacioRendiment modificarCoordenadesINomDeWaypoints(
			ComprovacioRendiment comprovacioRendimentTmp) {
		// Creació de l'Scanner
		Scanner sc = new Scanner(System.in);
		// Recorrem la llista d'array
		for (Waypoint_Dades wd : comprovacioRendimentTmp.llistaArrayList) {
			// Si el seu ID es parell, mostrem el seu nom i coordenades i demanem de nous
			if (wd.getId() % 2 == 0) {
				System.out.println("------ Modificar el waypoint amb id = " + wd.getId() + " ------");
				// Mostrem el nom actual
				System.out.println("Nom actual: " + wd.getNom());
				// Demanem el nou nom
				System.out.print("Nom nou: ");
				String nouNom = sc.nextLine();
				// Modifiquem el nom
				wd.setNom(nouNom);
				System.out.println("Nom actual: " + wd.getNom());
				// Mostrem les coordenade actuals
				System.out.print("Coordenades actuals: ");
				for (int i = 0; i < wd.getCoordenades().length; i++) {
					System.out.print(wd.getCoordenades()[i] + " ");
				}
				System.out.println();
				// Demanem les noves coordenades separades per espais en blanc
				System.out.print("Coordenades noves (format: 1 13 7): ");
				String novesCoordenades = sc.nextLine();
				// Inicialitzem variable per saber si numero de coordenades es valid
				boolean numeroCoordenades = false, coordenadesValid = false;
				// Ens asegurrem que només fica 3 valors separats per espais
				String coordenades[] = novesCoordenades.split(" ");
				// System.out.println("Longitud coordenades " + coordenades.length);
				// Si la longitud de coordenades no es igual a 3, mostrem un error i tornem a
				// demanar
				if (coordenades.length != 3) {
					System.out.println("ERROR introduir 3 paràmetres separats per 1 espai en blanc. Has introduït "
							+ coordenades.length + " paràmetres");
				} else {
					numeroCoordenades = true;
				}
				// Comprovem que les coordenades son integers
				int cont = 0;
				for (int i = 0; i < coordenades.length; i++) {
					// Si la coordenada es enter, incrementem el contador
					// En cas contrari mostrem que no es valida
					if (Cadena.stringIsInt(coordenades[i])) {
						cont++;
					} else {
						System.out.println("ERROR: coordenada " + coordenades[i] + " no vàlida.");
					}
				}
				// Comprobem el contador
				if (cont == 3) {
					coordenadesValid = true;
				}
				// Reiniciem el contador
				cont = 0;
				while (!numeroCoordenades || !coordenadesValid) {
					// Demanem les noves coordenades separades per espais en blanc
					System.out.print("Coordenades noves (format: 1 13 7): ");
					novesCoordenades = sc.nextLine();
					// Ens asegurrem que només fica 3 valors separats per espais
					coordenades = novesCoordenades.split(" ");
					// System.out.println("Longitud coordenades " + coordenades.length);
					// Si la longitud de coordenades no es igual a 3, mostrem un error i tornem a
					// demanar
					if (coordenades.length != 3) {
						System.out.println("ERROR introduir 3 paràmetres separats per 1 espai en blanc. Has introduït "
								+ coordenades.length + " paràmetres");
					} else {
						numeroCoordenades = true;
					}
					// Comprovem que les coordenades son integers
					cont = 0;
					for (int i = 0; i < coordenades.length; i++) {
						// Si la coordenada es enter, incrementem el contador
						// En cas contrari mostrem que no es valida
						if (Cadena.stringIsInt(coordenades[i])) {
							cont++;
						} else {
							System.out.println("ERROR: coordenada " + coordenades[i] + " no vàlida.");
						}
					}
					// Comprobem el contador
					if (cont == 3) {
						coordenadesValid = true;
					}
					// Reiniciem el contador
					cont = 0;
				}
				// Si les coordenades son valide les canviem
				if (coordenadesValid) {
					int coordenadesInt[] = new int[3];
					int j = 0;
					for (int i = 0; i < coordenades.length; i++) {
						coordenadesInt[j] = Integer.parseInt(coordenades[i]);
						j++;
					}
					wd.setCoordenades(coordenadesInt);
				}
				System.out.println("|");

			}
		}
		return comprovacioRendimentTmp;
	}

	/**
	 * visualizarWaypointsOrdenats
	 * 
	 * mostrar la llista de waypoints
	 * 
	 * @param comprovacioRendimentTmp
	 */
	public static void visualizarWaypointsOrdenats(ComprovacioRendiment comprovacioRendimentTmp) {
		// Ordenem els waypoint
		Collections.sort(comprovacioRendimentTmp.llistaArrayList);
		// Els mostrem
		for (Waypoint_Dades w : comprovacioRendimentTmp.llistaArrayList) {
			System.out.println(w.toString());
		}
	}

	/**
	 * waypointsACertaDistanciaMaxDeLaTerra
	 * 
	 * Mostrar els waypoints que es troben a una distància màxima de la Terra i
	 * retornar-los ordenats de menor a major distancia
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 */
	public static void waypointsACertaDistanciaMaxDeLaTerra(ComprovacioRendiment comprovacioRendimentTmp) {
		// Declaració de l'Scanner
		Scanner sc = new Scanner(System.in);
		// Demanem la distancia màxima fins la qual volem buscar waypoints
		System.out.print("Distància màxima de la Terra: ");
		String distanciaMaxima = sc.next();
		// Verifiquem que s'ha donat un enter
		if (Cadena.stringIsInt(distanciaMaxima)) {
			// Transformem el String a enter
			int distanciaMaximaInt = Integer.parseInt(distanciaMaxima);
			// Mostrem els waypoints a partir de la distancia donada
			// Ordenem
			for (Waypoint_Dades w : comprovacioRendimentTmp.llistaArrayList) {
				int totalCoordenades = 0;
				// Calculem les coordenades
				for (int i = 0; i < w.getCoordenades().length; i++) {
					totalCoordenades += Math.pow(w.getCoordenades()[i], 2);
				}
				// Si el total es menor a la distancia maxima el mostrem
				if (totalCoordenades <= distanciaMaximaInt) {
					System.out.println(w.toString());
				}
			}
		} else {
			System.out.println("No s'ha introduït un número enter com a distància màxima");
		}

	}

	/**
	 * inicialitzarDadesWaypoints
	 * 
	 * Ficar waypoints a una llista de waypoints donada
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 * @return objecte comprovacioRendimentTmp
	 */
	public static ComprovacioRendiment inicialitzarDadesWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		// Comprobem que la llista de waypoints esta buida
		// Si no ho esta, la buidem
		// En cas contrari introduiem els waypoint
		if (comprovacioRendimentTmp.llistaWaypoints.size() > 0) {
			comprovacioRendimentTmp.llistaWaypoints.clear();
		}
		comprovacioRendimentTmp.llistaWaypoints.push(new Waypoint_Dades(0, "Òrbita de la Terra", new int[] { 0, 0, 0 },
				true, LocalDateTime.parse("21-10-2020 01:10", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 0));
		comprovacioRendimentTmp.llistaWaypoints.push(new Waypoint_Dades(1, "Punt Lagrange Terra-LLuna",
				new int[] { 1, 1, 1 }, true, LocalDateTime.parse("21-10-2020 01:00", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 6));
		comprovacioRendimentTmp.llistaWaypoints.push(new Waypoint_Dades(2, "Òrbita de la LLuna", new int[] { 2, 2, 2 },
				true, LocalDateTime.parse("21-10-2020 00:50", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 1));
		comprovacioRendimentTmp.llistaWaypoints.push(new Waypoint_Dades(3, "Òrbita de Mart", new int[] { 3, 3, 3 },
				true, LocalDateTime.parse("21-10-2020 00:40", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 0));
		comprovacioRendimentTmp.llistaWaypoints.push(new Waypoint_Dades(4, "Òrbita de Júpiter", new int[] { 4, 4, 4 },
				true, LocalDateTime.parse("21-10-2020 00:30", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 0));
		comprovacioRendimentTmp.llistaWaypoints.push(new Waypoint_Dades(5, "Punt Lagrange Júpiter-Europa",
				new int[] { 5, 5, 5 }, true, LocalDateTime.parse("21-10-2020 00:20", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 6));
		comprovacioRendimentTmp.llistaWaypoints.push(new Waypoint_Dades(6, "Òrbita de Europa", new int[] { 6, 6, 6 },
				true, LocalDateTime.parse("21-10-2020 00:10", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 0));
		comprovacioRendimentTmp.llistaWaypoints.push(new Waypoint_Dades(7, "Òrbita de Venus", new int[] { 7, 7, 7 },
				true, LocalDateTime.parse("21-10-2020 00:01", Data.formatter), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.formatter), 0));

		return comprovacioRendimentTmp;
	}

	/**
	 * nouWaypoint
	 * 
	 * Demanar a l'usuari dades per a donar d'alta un nou waypoint
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 * @return objecte comprovacioRendimentTmp
	 */
	public static ComprovacioRendiment nouWaypoint(ComprovacioRendiment comprovacioRendimentTmp) {
		// Declaració de l'Scanner
		Scanner sc = new Scanner(System.in);
		// Agafem el id més alt
		Waypoint_Dades wdMaxId = Collections.max(comprovacioRendimentTmp.llistaWaypoints,
				Comparator.comparing(wd -> wd.getId()));
		int maxId = wdMaxId.getId();
		// incrementem el maxId
		int idWaypoint = maxId++;

		// Demanem el nom
		System.out.print("Nom del waypoint: ");
		String nomWaypoint = sc.nextLine();

		// Inicialitzem un array coordenadade integer on guardarem les coordenades
		int coordenadesInt[] = null;
		// Demanem les coordenades a l'usuari
		System.out.print("Coordenades (format: 1 13 7): ");
		String novesCoordenades = sc.nextLine();
		// Inicialitzem variable per saber si numero de coordenades es valid
		boolean numeroCoordenades = false, coordenadesValid = false;
		// Ens asegurrem que només fica 3 valors separats per espais
		String coordenades[] = novesCoordenades.split(" ");
		// System.out.println("Longitud coordenades " + coordenades.length);
		// Si la longitud de coordenades no es igual a 3, mostrem un error i tornem a
		// demanar
		if (coordenades.length != 3) {
			System.out.println("ERROR introduir 3 paràmetres separats per 1 espai en blanc. Has introduït "
					+ coordenades.length + " paràmetres");
		} else {
			numeroCoordenades = true;
		}
		// Comprovem que les coordenades son integers
		int cont = 0;
		for (int i = 0; i < coordenades.length; i++) {
			// Si la coordenada es enter, incrementem el contador
			// En cas contrari mostrem que no es valida
			if (Cadena.stringIsInt(coordenades[i])) {
				cont++;
			} else {
				System.out.println("ERROR: coordenada " + coordenades[i] + " no vàlida.");
			}
		}
		// Comprobem el contador
		if (cont == 3) {
			coordenadesValid = true;
		}
		// Reiniciem el contador
		cont = 0;
		while (!numeroCoordenades || !coordenadesValid) {
			// Demanem les noves coordenades separades per espais en blanc
			System.out.print("Coordenades noves (format: 1 13 7): ");
			novesCoordenades = sc.nextLine();
			// Ens asegurrem que només fica 3 valors separats per espais
			coordenades = novesCoordenades.split(" ");
			// System.out.println("Longitud coordenades " + coordenades.length);
			// Si la longitud de coordenades no es igual a 3, mostrem un error i tornem a
			// demanar
			if (coordenades.length != 3) {
				System.out.println("ERROR introduir 3 paràmetres separats per 1 espai en blanc. Has introduït "
						+ coordenades.length + " paràmetres");
			} else {
				numeroCoordenades = true;
			}
			// Comprovem que les coordenades son integers
			cont = 0;
			for (int i = 0; i < coordenades.length; i++) {
				// Si la coordenada es enter, incrementem el contador
				// En cas contrari mostrem que no es valida
				if (Cadena.stringIsInt(coordenades[i])) {
					cont++;
				} else {
					System.out.println("ERROR: coordenada " + coordenades[i] + " no vàlida.");
				}
			}
			// Comprobem el contador
			if (cont == 3) {
				coordenadesValid = true;
			}
			// Reiniciem el contador
			cont = 0;
		}
		// Si les coordenades son valide les canviem
		if (coordenadesValid) {
			coordenadesInt = new int[3];
			int j = 0;
			for (int i = 0; i < coordenades.length; i++) {
				coordenadesInt[j] = Integer.parseInt(coordenades[i]);
				j++;
			}
			// wd.setCoordenades(coordenadesInt);
		}

		// Demanem si està actiu
		boolean actiuWaypoint = false;
		boolean actiuValid = false;
		System.out.print("Actiu? (format: true|false): ");
		String actiu = sc.nextLine();
		// Declarem un collator
		Collator comparador = Collator.getInstance();
		comparador.setStrength(Collator.PRIMARY);
		// Si es valid deixem de demanar
		if (comparador.compare("true", actiu) == 0) {
			actiuWaypoint = true;
			actiuValid = true;
		} else if (comparador.compare("false", actiu) == 0) {
			actiuWaypoint = false;
			actiuValid = true;
		} else {
			System.out.println("ERROR: actiu " + actiu + " no vàlid");
		}
		// Sino seguim demanant
		while (!actiuValid) {
			System.out.print("Actiu? (format: true|false): ");
			actiu = sc.nextLine();
			// Declarem un collator
			comparador = Collator.getInstance();
			comparador.setStrength(Collator.PRIMARY);
			// Si es valid deixem de demanar
			if (comparador.compare("true", actiu) == 0) {
				actiuWaypoint = true;
				actiuValid = true;
			} else if (comparador.compare("false", actiu) == 0) {
				actiuWaypoint = false;
				actiuValid = true;
			} else {
				System.out.println("ERROR: actiu " + actiu + " no vàlid");
			}
		}

		// Data de creació es la data actual del sistema
		LocalDateTime actual = LocalDateTime.now();
		// Formatador de dates
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		// Formatejem la data
		String actualFormated = actual.format(formatter);
		// Transformem la data a LocalDateTime
		LocalDateTime actualLDT = LocalDateTime.parse(actualFormated, formatter);

		// Demanem la data a l'usuari
		LocalDateTime dataLDT = null;
		boolean dataValid = false;
		System.out.print("Data d'anulació (DD-MM-AAAA): ");
		String data = sc.nextLine();
		// Comprobem que es una data valida
		if (!Data.esData(data)) {
			System.out.println("ERROR: data de creació " + data + " no vàlida");
		} else {
			dataValid = true;
		}
		// Si no es vàlida seguim demanant
		while (!dataValid) {
			System.out.print("Data d'anulació (DD-MM-AAAA): ");
			data = sc.nextLine();
			// Comprobem que es una data valida
			if (!Data.esData(data)) {
				System.out.println("ERROR: data de creació " + data + " no vàlida");
			} else {
				dataValid = true;
			}
		}
		// Convertim la data a localdatetime
		if (dataValid) {
			// Concatenem hores i minuts
			data += " 00:00";
			// Formatejem la data
			//String dataFormated = data.format(formatter);
			// Transformem la data a LocalDateTime
			dataLDT = LocalDateTime.parse(data, formatter);
		}
		
		// Data d'anulació sera la data actual del sistema
		
		// Escollim el tipus
		boolean tipusValid = false;
		System.out.println("Tipus de waypoints disponibles:");
		// Els mostrem
		for (int i = 0; i < TipusWaypoint.TIPUS_WAYPOINT.length; i++) {
			System.out.println(i + ": " + TipusWaypoint.TIPUS_WAYPOINT[i]);
		}
		System.out.print("Tipus de waypoint (format: 3): ");
		int tipus = sc.nextInt();
		// Comprobem que es troba entre les opcions
		if (tipus >= 0 && tipus < TipusWaypoint.TIPUS_WAYPOINT.length) {
			tipusValid = true;
		} else {
			System.out.println("ERROR: introduir 1 número dels visualitzats en la llista anterior. El número " + tipus + " no existeix");
		}
		// Si no es valid tornem a demanar
		while (!tipusValid) {
			System.out.print("Tipus de waypoint (format: 3): ");
			tipus = sc.nextInt();
			// Comprobem que es troba entre les opcions
			if (tipus >= 0 && tipus < TipusWaypoint.TIPUS_WAYPOINT.length) {
				tipusValid = true;
			} else {
				System.out.println("ERROR: introduir 1 número dels visualitzats en la llista anterior. El número " + tipus + " no existeix");
			}
		}
		
		// Si les dades son valides creem el objecte i l'afegim
		if (coordenadesValid && actiuValid && dataValid && tipusValid) {
			comprovacioRendimentTmp.llistaWaypoints.add(new Waypoint_Dades(idWaypoint, nomWaypoint, coordenadesInt, actiuWaypoint, actualLDT, dataLDT ,actualLDT, tipus));
		}
		return comprovacioRendimentTmp;
	}
	
	/**
	 * waypointsVsTipus
	 * 
	 * visualitzar els waypoints versus un tipus
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 * @return objecte comprovacioRendimentTmp
	 */
	public static ComprovacioRendiment waypointsVsTipus(ComprovacioRendiment comprovacioRendimentTmp) {
		// Declarem l'Scanner
		Scanner sc = new Scanner(System.in);
		// Mostrem els tipus per pantalla
		System.out.println("Tipus de waypoints disponibles:");
		// Els mostrem
		for (int i = 0; i < TipusWaypoint.TIPUS_WAYPOINT.length; i++) {
			System.out.println(i + ": " + TipusWaypoint.TIPUS_WAYPOINT[i]);
		}
		System.out.print("Tipus de waypoint (format: 3): ");
		int tipus = sc.nextInt();
		// Inicialitzem la llista
		HashSet<Waypoint_Dades> llistaA = new HashSet<Waypoint_Dades>();
		HashSet<Waypoint_Dades> llistaB = new HashSet<Waypoint_Dades>();
		// Fiquem tots els waypoints a llistaA
		for (Waypoint_Dades wd : comprovacioRendimentTmp.llistaWaypoints) {
			llistaA.add(wd);
		}
		// Recorrem llistaA i si son del tipus donat l'afegim a llistaB
		for (Waypoint_Dades wd : llistaA) {
			if (wd.getTipus() == tipus) {
				// L'afegim a la llistaB
				llistaB.add(wd);
				System.out.println("Esborrat el waypoint amb id = " + wd.getId() + " del tipus " + wd.getTipus() + ". Waypoint llistaA --> llistaB.");
			}
		}
		// Esborrem els waypoints
		llistaA.removeAll(llistaB);
		// Mostrem la llistaB
		System.out.println("LlistaB de waypoints del tipus " + tipus + ":");
		for (Waypoint_Dades wd: llistaB) {
			System.out.println("id " + wd.getId() + ": " + wd.getNom() + ", de tipus " + tipus);
		}
		return comprovacioRendimentTmp;
	}
	
	/**
	 * numWaypointsVsTipus
	 * 
	 * visualitzar el nº de waypoints versus el seu tipus
	 * 
	 * @param comprovacioRendimentTmp objecte ComprovacioRendiment
	 * @return objecte comprovacioRendimentTmp
	 */
	public static ComprovacioRendiment numWaypointsVsTipus(ComprovacioRendiment comprovacioRendimentTmp) {
		// Creem un map
		HashMap hmWaypoints = new HashMap();
		// Afegim els tipus amb un contador
		for (int i = 0; i < TipusWaypoint.TIPUS_WAYPOINT.length; i++) {
			hmWaypoints.put(TipusWaypoint.TIPUS_WAYPOINT[i], 0);
		}
		// Fiquem tots els waypoints en un 1 set
		HashSet<Waypoint_Dades> setWaypoints = new HashSet<Waypoint_Dades>();
		// Recorrem els waypoint
		for (Waypoint_Dades wd : comprovacioRendimentTmp.llistaWaypoints) {
			// L'afegim al hashset
			setWaypoints.add(wd);
			// Incrementem el valor del tipus
			int valor = (int) hmWaypoints.get(TipusWaypoint.TIPUS_WAYPOINT[wd.getTipus()]);
			hmWaypoints.put(TipusWaypoint.TIPUS_WAYPOINT[wd.getTipus()], valor + 1);
		}
		// Mostrem el resultat
		Set set = hmWaypoints.entrySet();
		Iterator i = set.iterator();
		while(i.hasNext()) {
			Map.Entry me = (Map.Entry)i.next();
			System.out.println("Tipus (" + me.getKey() + "): " + me.getValue() + " naus");
		}
		return comprovacioRendimentTmp;
	}
}
