package com.daw.alexfrancisco.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class ComprovacioRendiment {
	// Atributs
	int[] coordenadesTmp = null;
	List<Waypoint_Dades> llistaArrayList;
	List<Waypoint_Dades> llistaLinkedList;
	Waypoint_Dades wtmp;
	public Deque<Waypoint_Dades> pilaWaypoints;
	public ArrayList<Ruta_Dades> llistaRutes;
	public LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes;
	public LinkedList<Waypoint_Dades> llistaWaypoints;
	public LinkedHashMap<Integer, Waypoint_Dades> mapaWaypoints;
	
	/**
	 * @param coordenadesTmp
	 * @param llistaArrayList
	 * @param llistaLinkedList
	 */
	public ComprovacioRendiment() {
		// Inicialitzem els arrays
		this.coordenadesTmp = new int[] {0, 0, 0};
		this.llistaArrayList = new ArrayList<Waypoint_Dades>();
		this.llistaLinkedList = new LinkedList<Waypoint_Dades>();
		this.wtmp = null;
		this.pilaWaypoints = new ArrayDeque<Waypoint_Dades>();
		this.llistaRutes = new ArrayList<Ruta_Dades>();
		this.mapaLinkedDeRutes = new LinkedHashMap<Integer, Ruta_Dades>();
		this.llistaWaypoints = new LinkedList<Waypoint_Dades>();
		this.mapaWaypoints = new LinkedHashMap<Integer, Waypoint_Dades>();
	}
}
