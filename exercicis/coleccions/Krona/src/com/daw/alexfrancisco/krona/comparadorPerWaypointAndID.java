package com.daw.alexfrancisco.krona;

import java.util.Comparator;
import java.util.LinkedHashMap;

public class comparadorPerWaypointAndID implements Comparator {

	public LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes;
	
	public comparadorPerWaypointAndID(LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes) {
		this.mapaLinkedDeRutes = mapaLinkedDeRutes;
	}
	@Override
	public int compare(Object ob1, Object ob2) {
		Comparable valorA = (Comparable) mapaLinkedDeRutes.get(ob1);
		Comparable valorB = (Comparable) mapaLinkedDeRutes.get(ob2);
		return valorA.compareTo(valorB);
	}

}
