package exercici_6;

import java.util.Comparator;
import java.util.Collections;

public class EntradaAgenda implements Comparable<EntradaAgenda> {
	// Atributs
	private String nom;
	private String primerCognom;
	private String segonCognom;
	private int telefon;
	private String email;
	private int mobil;

	/**
	 * @param nom
	 * @param primerCognom
	 * @param segonCognom
	 * @param telefon
	 * @param email
	 * @param mobil
	 */
	public EntradaAgenda(String nom, String primerCognom, String segonCognom, int telefon, String email, int mobil) {
		this.nom = nom;
		this.primerCognom = primerCognom;
		this.segonCognom = segonCognom;
		this.telefon = telefon;
		this.email = email;
		this.mobil = mobil;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrimerCognom() {
		return primerCognom;
	}

	public void setPrimerCognom(String primerCognom) {
		this.primerCognom = primerCognom;
	}

	public String getSegonCognom() {
		return segonCognom;
	}

	public void setSegonCognom(String segonCognom) {
		this.segonCognom = segonCognom;
	}

	public int getTelefon() {
		return telefon;
	}

	public void setTelefon(int telefon) {
		this.telefon = telefon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getMobil() {
		return mobil;
	}

	public void setMobil(int mobil) {
		this.mobil = mobil;
	}

	/**
	 * compare
	 * 
	 * Comparem dos entradas d'agenda a partir dels cognoms i despres el nom
	 * 
	 * @param primera EntradaAgenda
	 * @param segona  EntradaAgenda
	 */
	@Override
	public int compareTo(EntradaAgenda ea) {
		// Comparem el primer cognom
		int result = this.getPrimerCognom().compareTo(ea.getPrimerCognom());
		// Si son iguals comparem per el segon cognom
		if (result == 0) {
			result = this.getSegonCognom().compareTo(ea.getSegonCognom());
		}
		// Si segueixen sent iguals comparem per el nom
		if (result == 0) {
			result = this.getNom().compareTo(ea.getNom());
		}
		return result;
	}

	@Override
	public String toString() {
		return "EntradaAgenda [nom=" + nom + ", primerCognom=" + primerCognom + ", segonCognom=" + segonCognom
				+ ", telefon=" + telefon + ", email=" + email + ", mobil=" + mobil + "]";
	}
}
