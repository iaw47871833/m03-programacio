package exercici_6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Exercici_6 {

	public static void main(String[] args) {
		// Creacio de la llista
		List<EntradaAgenda> llistaContacte = new ArrayList<EntradaAgenda>();
		// Creem les entrades d'agenda
		EntradaAgenda ea1 = new EntradaAgenda("Xavi", "Francisco", "Tinaya", 913243921, "alex@alex.es", 603694858);
		EntradaAgenda ea2 = new EntradaAgenda("Maria", "Francisco", "Tinaya", 913243921, "maria@maria.es", 694838292);
		EntradaAgenda ea3 = new EntradaAgenda("Paula", "Diaz", "Castaño", 948328211, "paula@paula.es", 627391843);
		EntradaAgenda ea4 = new EntradaAgenda("Pere", "Martinez", "Santos", 929049322, "pere@pere.es", 661682932);
		EntradaAgenda ea5 = new EntradaAgenda("Rosa", "Lopez", "Valdes", 913243921, "rosa@rosa.es", 602958432);
		
		// Afegim els registres
		llistaContacte.add(ea1);
		llistaContacte.add(ea2);
		llistaContacte.add(ea3);
		llistaContacte.add(ea4);
		llistaContacte.add(ea5);
		
		System.out.println("Llista ordenada de contactes");
		// Ordenem els contactes
		Collections.sort(llistaContacte);
		// Mostrem la llista
		for (EntradaAgenda ea: llistaContacte) {
			System.out.println(ea.toString());
		}
	}

}
