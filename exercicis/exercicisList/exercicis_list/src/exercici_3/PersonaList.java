package exercici_3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PersonaList {
	// Constant per identificar l'edat dels menors
	public static final int MAJOR_EDAT = 18;
	
	/**
	 * menorsEdat
	 * 
	 * Llistar els menors d'edat
	 * 
	 * @param llistaPersona llista de persones
	 * @return llista de menors
	 */
	public static List<Persona> menorsEdat(List<Persona> llistaPersona) {
		// Inicialitzem la llista
		List<Persona> menors = new ArrayList<Persona>();
		// Recorrem la llista de persones
		for (Persona p : llistaPersona) {
			// Si la seva edat es menor a 18, mostrem la seva informacio
			if (p.getEdat() < MAJOR_EDAT) {
				menors.add(p);
			}
		}
		return menors;
	}
	
	/**
	 * menorsEdatOrdenats
	 * 
	 * Llistar els menors d'edat a partir d'una llista ordenada de persones
	 * 
	 * @param llistaMenorsOrd
	 * @return llista de menors
	 */
	public static List<Persona> menorsEdatOrdenats(List<Persona> llistaPersona) {
		// Variable inicialitzada a 0 que incrementara per cada menor detectat
		int contador = 0;
		boolean esMenor = true;
		// Inicialitzem la llista de menors
		List<Persona> menors = new ArrayList<>();
		Iterator<Persona> it = llistaPersona.iterator();
		// Recorrem la llista mentre hi hagi elements al iterador i hi hagi menors
		while (it.hasNext() && esMenor) {
			// Si es menor d'edat incrementem el contador
			// En cas contrari, canviem el valor de esMenor per sortir del bucle
			if (it.next().getEdat() > MAJOR_EDAT) {
				esMenor = false;
			} else {
				contador++;
			}
			// Guardem desde la primera posicio de la llista fins al contador
			menors = llistaPersona.subList(0, contador);
		}
		return menors;
	}
}
