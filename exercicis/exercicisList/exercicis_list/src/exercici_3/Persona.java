package exercici_3;

import java.util.Calendar;

public class Persona implements Comparable<Persona> {
	// Atributs
	// Nom
	private String nom;
	// Cognoms
	private String cognoms;
	// Data naixement
	private Calendar data_naixement;
	// DNI 
	private String dni;
	
	/**
	 * @param nom
	 * @param cognoms
	 * @param data_naixement
	 * @param dni
	 */
	public Persona(String nom, String cognoms, Calendar data_naixement, String dni) {
		this.nom = nom;
		this.cognoms = cognoms;
		this.data_naixement = data_naixement;
		this.dni = dni;
	}
	
	/**
	 * getEdat
	 * Métode per saber l'edat de la persona
	 * 
	 * @param args Not used
	 * @return edad
	 */
	public int getEdat() {
		// Guardem el dia actual
		Calendar avui = Calendar.getInstance();
		// Calculem l'edat a partir de l'any actual i l'any de naixement
		int edat = avui.get(Calendar.YEAR) - this.getData_naixement().get(Calendar.YEAR);
		// Si el mes actual es menor al de naixement, restem 1
		// ATENCIÓ: Calendar.MONTH va del 0-11
		if (avui.get(Calendar.MONTH) < this.getData_naixement().get(Calendar.MONTH)) {
			edat--;
		}
		// Si estem en el mateix mes de naixement pero el dia es anterior al dia de naixement
		// restem 1 a la edat
		else if ((avui.get(Calendar.MONTH) == this.getData_naixement().get(Calendar.MONTH)) &&
				(avui.get(Calendar.DAY_OF_MONTH) < this.getData_naixement().get(Calendar.DAY_OF_MONTH))) {
			edat--;
		}
		return edat;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the cognoms
	 */
	public String getCognoms() {
		return cognoms;
	}

	/**
	 * @param cognoms the cognoms to set
	 */
	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}

	/**
	 * @return the data_naixement
	 */
	public Calendar getData_naixement() {
		return data_naixement;
	}

	/**
	 * @param data_naixement the data_naixement to set
	 */
	public void setData_naixement(Calendar data_naixement) {
		this.data_naixement = data_naixement;
	}

	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}

	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", cognoms=" + cognoms + ", data_naixement=" + data_naixement.get(Calendar.DAY_OF_MONTH) +
				"-" + (data_naixement.get(Calendar.MONTH) + 1) + "-" + data_naixement.get(Calendar.YEAR) + ", dni=" + dni
				+ "]";
	}
	
	// Compareto
	@Override
	public int compareTo(Persona o) {
		// Comparem les edats
		int result = this.getEdat() - o.getEdat();
		// En cas de que siguin iguals comparem els cognoms
		if (result == 0){
			result = this.getCognoms().compareTo(o.getCognoms());
		}
		return result;
	}

}
