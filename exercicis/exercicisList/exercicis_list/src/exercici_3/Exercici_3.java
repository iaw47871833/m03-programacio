package exercici_3;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class Exercici_3 {

	public static void main(String[] args) {
		// Creem una data amb calendar
		Calendar data_naixement1 = Calendar.getInstance();
		// Cambiem els valors
		data_naixement1.set(1998, 9, 29);
		Calendar data_naixement2 = Calendar.getInstance();
		data_naixement2.set(2000, 10, 5);
		Calendar data_naixement3 = Calendar.getInstance();
		data_naixement3.set(2010, 7, 13);
		Calendar data_naixement4 = Calendar.getInstance();
		data_naixement4.set(2007, 3, 20);
		Calendar data_naixement5 = Calendar.getInstance();
		data_naixement5.set(2007, 3, 20);
		// Creem les persones
		Persona p1 = new Persona("Alex", "Francisco", data_naixement1, "47871833R");
		Persona p2 = new Persona("Ana", "Francisco", data_naixement2, "8738291Ñ");
		Persona p3 = new Persona("Carlos", "Garcia", data_naixement3, "43141311B");
		Persona p4 = new Persona("David", "Fernandez", data_naixement4, "48413235K");
		Persona p5 = new Persona("Marta", "Arias", data_naixement5, "48413235K");
		// Afegim les persones a la llista
		List<Persona> llistaPersona = new ArrayList<Persona>();
		llistaPersona.add(p1);
		llistaPersona.add(p2);
		llistaPersona.add(p3);
		llistaPersona.add(p4);
		llistaPersona.add(p5);
		// Cridem al métode menorsEdat
		System.out.println("Llista menors");
		System.out.println(PersonaList.menorsEdat(llistaPersona));
		
		// Ordenem la llista
		Collections.sort(llistaPersona);
		// Cridem al métode menorsEdatOrdenats
		System.out.println("Llista menors ordenats");
		System.out.println(PersonaList.menorsEdatOrdenats(llistaPersona));
	}

}
