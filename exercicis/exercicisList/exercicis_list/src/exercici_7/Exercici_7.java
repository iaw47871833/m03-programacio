package exercici_7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Exercici_7 {

	public static void main(String[] args) {
		// Llista per guardar la baralla
		List<String> baralla = construirBaralla();
		// Barregem la baralla
		Collections.shuffle(baralla);
		// Repartim als jugadors
		repartir(baralla, Integer.parseInt(args[0]), Integer.parseInt(args[1]));
	}
	
	/**
	 * construirBaralla
	 * 
	 * Metode per construir una baralla americana de cartes
	 * @param args Not used
	 * @return baralla americana
	 */
	public static List<String> construirBaralla() {
		// Inicialitzem la llista
		List<String> cartes = new ArrayList<String>();
		// Inicialitzem els arrays
		String pals[] = {"cors", "piques", "diamants", "trevols"};
		String valors[] = {"As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
		// Bucle per recorrer els pals
		for (int i = 0; i < pals.length; i++) {
			// Bucle per recorrer els valors
			for (int j = 0; j < valors.length; j++) {
				// Creem la carta
				String carta = pals[i] + "-" + valors[j];
				// L'afegim a la llista de cartes
				cartes.add(carta);
			}
		}
		// Retornem les cartes
		return cartes;
	}
	
	/**
	 * construirMa
	 * 
	 * Donada una baralla de cartes repartir n cartes a m jugadors
	 * 
	 * @param baralla llista de cartes
	 * @param nCartes numero de cartes
	 * @return ma de nCartes
	 */
	public static List<String> construirMa (List<String> baralla, int nCartes) {
		// Creem una llista de ncartes que va desde el final
		List<String> sublist = baralla.subList(baralla.size() - nCartes, baralla.size());
		// L'assignem a la llista "ma"
		List<String> ma = new ArrayList<String>(sublist);
		// Suprimim les cartes de la baralla (nCartes fins al final), per evitar donar dues vegades la mateixa carta
		baralla.subList(baralla.size() - nCartes, baralla.size()).clear();
		// Retornem la ma
		return ma;
	}
	
	/**
	 * repartir
	 * 
	 * Métode per repartir una ma als jugadors
	 * 
	 * @param baralla llista de cartes
	 * @param nCartes numero de cartes a repartir
	 * @param nJugadors numero de jugadors
	 */ 
	 public static void repartir(List<String> baralla, int nCartes, int nJugadors) {
		 // Comprobem que el numero de cartes i jugadors es proporcional
		 // Si no es poden repartir el numero de cartes mostrem un missatge
		 // En cas contrari mostrem les cartes de cada jugador
		 if (nCartes * nJugadors > baralla.size()) {
			 System.out.println("El número de jugadors es superior a la quantitat de cartes a repartir");
		 } else {
			 // Bucle per recorrer els jugadors
			 for(int i = 0; i < nJugadors; i++) {
				 // Cridem al metode repartir
				 List<String> maJugador = construirMa(baralla, nCartes);
				 // Mostrem les cartes del jugador
				 System.out.println("Mà jugador " + (i + 1) + ":");
				 for(String carta: maJugador) {
					 System.out.println(carta);
				 }
			 }
		 }
	 }

}
