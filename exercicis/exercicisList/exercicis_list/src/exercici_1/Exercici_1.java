package exercici_1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Exercici_1 {

	public static void main(String[] args) {
		// Creem la list de numeros
		List<Integer> numerosList = new ArrayList<Integer>();
		// Bucle per recorrer l'array d'arguments
		for (int i = 0; i < args.length; i++) {
			// Convertim l'String a enter
			int numInt = Integer.parseInt(args[i]);
			// L'afegim a la llista
			numerosList.add(numInt);
		}
		// Llegi el número d'elements a la llista
		System.out.println("Número d'elements a la llista: " + numerosList.size());
		// Bucle per recorrer l'arraylist
		for (int i = 0; i < numerosList.size(); i++) {
			// Guardem el número
			int numeroActual = numerosList.get(i);
			// Guardem el quadrat
			int quadrat = (int)(Math.pow(numeroActual, 2));
			// Canviem el valor de la posicio "i" per el quadrat
			numerosList.set(i, quadrat);
		}
		// Recorrem l'array amb iterator
		Iterator<Integer> it = numerosList.iterator();
		// Mentre hi hagi elements el mostrem
		while(it.hasNext()) {
			// Si el valor es major a 100 l'eliminem
			if (it.next() > 100) {
				it.remove();
			}
		}
		// Ordenem l'array amb collections
		Collections.sort(numerosList);
		// Mostrem els elements per pantalla
		for (Integer element: numerosList) {
			System.out.println("element: " + element);
		}
		
		// Exercici 2
		// Creem una nova llista
		List<Integer> numerosList2 = new ArrayList<Integer>();
		// Afegim elements
		numerosList2.add(20);
		numerosList2.add(15);
		numerosList2.add(35);
		numerosList2.add(7);
		// Concatenem la llista a la llista anterior
		numerosList.addAll(numerosList2);
		System.out.println("-----------------");
		// Recorrem la segon llista
		for (int element : numerosList2) {
			// Comprovem si la primera llista conté aquest element
			if (numerosList.contains(element)) {
				System.out.println("La llista conté el número " + element);
			}
		}
		// Eliminem els elements de la segona llista
		numerosList2.clear();
		// Comprovem que esta buida
		if (numerosList2.isEmpty()) {
			System.out.println("La llista SI esta buida");
		} else {
			System.out.println("La llista NO esta buida");
		}
	}

}
