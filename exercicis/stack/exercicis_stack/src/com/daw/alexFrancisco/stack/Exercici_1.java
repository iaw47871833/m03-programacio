package com.daw.alexFrancisco.stack;

import java.util.ArrayDeque;
import java.util.Deque;

public class Exercici_1 {
	public static void main(String[] args) {
		// Creem la pila de números enters
		Deque<Integer> stackDeque = new ArrayDeque<Integer>();
		
		// Creem els números
		Integer elem1 = new Integer(1);
		Integer elem2 = new Integer(2);
		Integer elem3 = new Integer(3);
		Integer elem4 = new Integer(4);
		
		// Afegim els números a la pila
		stackDeque.push(elem1);
		stackDeque.push(elem2);
		stackDeque.push(elem3);
		stackDeque.push(elem4);
		
		// Mostrem el deque original
		printDeque(stackDeque, "Deque (pila) original:");
		
		System.out.println();
		
		// Invertim la pila
		invertirPilaNoGeneric(stackDeque);
        System.out.println();
        invertirPila(stackDeque);
	}
	
	/**
	 * invertirPilaNoGeneric
	 * 
	 * Donada una pila no generica, invertirla
	 * 
	 * @param stack pila
	 * @return pila invertida
	 */
	public static void invertirPilaNoGeneric(Deque<Integer> stack) {
		// Pila on ficarem els valors de la pila original a la inversa
		Deque<Integer> inversStack = new ArrayDeque<Integer>();
		// Copia de la pila original
		Deque<Integer> cpStack = new ArrayDeque<Integer>();
		// Afegim els elements de la pila original a la copia
		for (Integer elem: stack) {
			cpStack.offer(elem);
		}
		// Afegim els elements de la copia començant per la cua
		while (!cpStack.isEmpty()) {
			inversStack.push(cpStack.pop());
		}
		// Imprimim la pila
		printDeque(inversStack, "Deque (pila) invertida amb mètode no genèric:");
		System.out.println(stack);
	}
	
	/**
	 * invertirPila
	 * 
	 * Donada una pila generica, invertirla
	 * 
	 * @param stack pila generica
	 */
	public static <T> void invertirPila(Deque<T> stack) {
		// Inicialitzem la pila inversa
		Deque<T> inversStack = new ArrayDeque<T>();
		// Afegim els elements de la pila original començant per la cua
		while (!stack.isEmpty()) {
			inversStack.push(stack.pop());
		}
		// Imprimim la pila
		printDeque(inversStack, "Deque (pila) invertida amb mètode genèric");
	}
	
	/**
	 * printDeque
	 * 
	 * Imprimir el deque donat
	 * 
	 * @param stack deque
	 * @text text a imprimir
	 */
	public static <T> void printDeque(Deque<T> stack, String text) {
        System.out.println(text);
        for (Object elem:stack){
            System.out.println(elem.toString());
        }
    }
}
