package com.daw.alexFrancisco.stack;

import java.util.ArrayDeque;
import java.util.Deque;

public class Exercici_2 {

	public static void main(String[] args) {
		String[] mina = {"<", ">", "(", "<", ">", ")", "<", "(", "<", ")"};
		
		minaDiamants(mina);


	}
	
	/**
	 * minaDiamants
	 * 
	 * Donada una mina de diamants, retornar el número de diamants blancs "<>" i
	 * diamansts negres "()" que existeixen
	 * 
	 * @param mina Deque d'String
	 */
	public static void minaDiamants(String mina[]) {
		// Inicialitzem la pila
		Deque<String> pilaDiamants = new ArrayDeque<String>();
		// Inicialitzem els contadors de diamant
        int diamantsBlanc = 0;
        int diamantsNegre = 0;
        // Recorrem la mina
        for(String elem : mina){
        	System.out.println("elemento: " + elem);
        	System.out.println("peek first: " + pilaDiamants.peekFirst());
            // Si el primer element de la llista es "<" i element es igual a ">", incrementem el contador blanc
        	// i eliminem el cap
            if (!pilaDiamants.isEmpty() && pilaDiamants.peekFirst().equals("<") && elem.equals(">")){ 
            	diamantsBlanc++;
                pilaDiamants.pop();
            }
            // Si el primer element de la llista es "(" i element es igual a ")", incrementem el contador negre
        	// i eliminem el cap
            else if (!pilaDiamants.isEmpty() && pilaDiamants.peekFirst().equals("(") && elem.equals(")")){ 
                	diamantsNegre++;
                    pilaDiamants.pop();
            }
            // Fiquem l'element a la pila
            else {
            	pilaDiamants.push(elem);
            }
        }
        System.out.println("A la mina hi ha " + diamantsBlanc + " diamants blancs i " + diamantsNegre + " diamants negres");
	}
}
