package com.daw.alexFrancisco.stack;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class Exercici_3 {

	public static void main(String[] args) {
		String[] expressioMatematica = {"25", "+", "3", "*", "(", "1", "+", "2", "+", "(", "30", "*", "4", ")", ")", "/","2"};
		comprovadorExpressions(expressioMatematica);

	}
	
	/**
	 * comprovadorExpressions
	 * 
	 * Donada una expressió matemàtica, comprovem que els parentesis son correctes
	 * @param expressio
	 */
	public static void comprovadorExpressions(String[] expressio) {
		// Inicialitzem la cua
		Queue<String> cuaParentesis = new LinkedList<String>();
		// Inicialitzem la pila
		Deque<String> pilaParentesis = new ArrayDeque<String>();
		// Recorrem la expressio
		for (String element: expressio) {
			// Si es "(" o ")" l'afegim a la cua
			if (element == "(" || element == ")") {
				cuaParentesis.offer(element);
			}
		}
		// Recorrem la cua
		for (String elem: cuaParentesis) {
			// Si es "(" el fiquem a la pila
			// Si es ")" eliminem un element de la pila
			if (elem == "(") {
				pilaParentesis.push(elem);
			} else if (elem == ")") {
				pilaParentesis.pop();
			}
		}
		// Si la pila esta buida, tot es correcte, en cas contrari hi ha algun error
		if (pilaParentesis.isEmpty() || cuaParentesis.isEmpty()) {
			System.out.println("L'expressió matemàtica es correcta");
		} else {
			System.out.println("L'expressió matemàtica té algún error");
		}
		
	}

}
