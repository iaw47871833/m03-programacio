package com.daw.alexFrancisco.stack;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class Exercici_4 {

	public static void main(String[] args) {
		// Declaració de les llistes
		Deque<String> cua1 = new LinkedList<String>();
		Deque<String> cua2 = new ArrayDeque<>();
		// Afegim els valors
		cua1.addAll(Arrays.asList("1", "2", "3", "4"));
		cua2.addAll(Arrays.asList("1", "2", "3", "4"));
		// Inversa amb iterator
		System.out.println("------ Cua amb iterator ------");
		// Cridem el métode
		Deque<String> cua1Inversa = inversaIterator(cua1);
		// El mostrem
		for (String elem : cua1Inversa) {
			System.out.println(elem);
		}
		// Inversa sense iterator
		System.out.println("------ Cua sense iterator ------");
		// Cridem el métode
		Deque<String> cua2Inversa = inversaNoIterator(cua2);
		// El mostrem
		for (String elem : cua2Inversa) {
			System.out.println(elem);
		}

	}

	public static <T> Deque<String> inversaIterator(Deque<String> cua) {
		// Inicialitzem l'array inverss
		Deque<String> dequeInvers = new LinkedList<String>();
		// Recorrer l'array amb Iterator
		Iterator<String> iteratorCua = cua.descendingIterator();
		// Mentre hi hagi elements, els afegim a la nova cua
		while (iteratorCua.hasNext()) {
			dequeInvers.offer(iteratorCua.next());
		}
		return dequeInvers;
	}
	
	public static <T> Deque<String> inversaNoIterator(Deque<String> cua) {
		// Inicialitzem l'array inverss
		Deque<String> dequeInvers = new ArrayDeque<>();
		// Recorrer l'array sense Iterator
		for (String elem : cua) {
			dequeInvers.push(elem);
		}
		return dequeInvers;
	}

}
